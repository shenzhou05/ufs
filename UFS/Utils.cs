﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.IO;


namespace UFS
{
    public static class Utils
    {
        public static Camera GetCurrentCamera()
        {
            // man, KSP could really just use a simple "get whatever the current camera is" method:
            return HighLogic.LoadedSceneIsEditor ?
                       EditorLogic.fetch.editorCamera :
                       (MapView.MapIsEnabled ?
                           PlanetariumCamera.Camera : FlightCamera.fetch.mainCamera);
        }

        public static string GetAssemblyFileVersion()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileVersion;
        }

        public static bool IsValidNumber(double input)
        {
            return !(double.IsInfinity(input) || double.IsNaN(input));
        }

        public static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }

        public static double RadiansToDegrees(double radians)
        {
            return radians * 180 / Math.PI;
        }

        public static bool IsValidVector(Vector3d vector)
        {
            return IsValidNumber(vector.x) &&
                   IsValidNumber(vector.y) &&
                   IsValidNumber(vector.z);
        }

        public static bool IsValidVector(Vector3 vector)
        {
            return IsValidNumber(vector.x) &&
                   IsValidNumber(vector.y) &&
                   IsValidNumber(vector.z);
        }

        public static bool IsValidRotation(Quaternion quaternion)
        {
            return IsValidNumber(quaternion.x) &&
                   IsValidNumber(quaternion.y) &&
                   IsValidNumber(quaternion.z) &&
                   IsValidNumber(quaternion.w);
        }

        /// <summary>
        /// Gets the *actual* list of attachnodes for a part.  Use as a replacement
        /// for the KSP API property part.attachNodes because that doesn't seem to
        /// reliably include the surface attached attachnodes all of the time.
        /// </summary>
        /// <param name="checkPart">part to get the nodes for</param>
        /// <returns>AttachNodes from this part to others</returns>
        private static IEnumerable<AttachNode> GetActualAttachedNodes(Part checkPart)
        {
            var returnList = new List<AttachNode>(checkPart.attachNodes);
            AttachNode srfNode = checkPart.srfAttachNode;
            if (!returnList.Contains(srfNode))
            {
                returnList.Add(srfNode);
            }
            return returnList;
        }

        /// <summary>
        ///   Fix the strange too-large or too-small angle degrees that are sometimes
        ///   returned by KSP, normalizing them into a constrained 360 degree range.
        /// </summary>
        /// <param name="inAngle">input angle in degrees</param>
        /// <param name="rangeStart">
        ///   Bottom of 360 degree range to normalize to.
        ///   ( 0 means the range [0..360]), while -180 means [-180,180] )
        /// </param>
        /// <returns>the same angle, normalized to the range given.</returns>
        public static double DegreeFix(double inAngle, double rangeStart)
        {
            double rangeEnd = rangeStart + 360.0;
            double outAngle = inAngle;
            while (outAngle > rangeEnd)
                outAngle -= 360.0;
            while (outAngle < rangeStart)
                outAngle += 360.0;
            return outAngle;
        }

        /// <summary>
        ///   Returns true if body a orbits body b, either directly or through
        ///   a grandparent chain.
        /// </summary>
        /// <param name="a">Does this body</param>
        /// <param name="b">Orbit around this body</param>
        /// <returns>True if a orbits b.  </returns>
        public static bool BodyOrbitsBody(CelestialBody a, CelestialBody b)
        {
            bool found = false;
            for (var curBody = a.referenceBody;
                 curBody != null && curBody != curBody.referenceBody; // reference body of Sun points to itself, weirdly.
                 curBody = curBody.referenceBody)
            {
                if (!curBody.name.Equals(b.name)) continue;

                found = true;
                break;
            }
            return found;
        }

        /// <summary>
        /// Return the parent body of this body, just like KSP's built-in referenceBody, except that
        /// it exhibits more sane behavior in the case of the Sun where there is no parent.  Default
        /// KSP's referenceBody will sometimes return null and sometimes return the Sun itself as
        /// the parent of the Sun.  This makes it always return null as the parent of the Sun no matter
        /// what.
        /// </summary>
        /// <param name="body">Body to get parent of (this will be hidden when called as an extension method)</param>
        /// <returns>parent body or null</returns>
        public static CelestialBody ExtensionGetParentBody(this CelestialBody body)
        {
            CelestialBody parent = body.referenceBody;
            if (parent == body)
                parent = null;
            return parent;
        }

        /// <summary>
        /// Determines if a given case insensitive name corresponds with a defined resource, and
        /// outputs the case sensitive name for easy access from Squad's lists.
        /// </summary>
        /// <param name="insensitiveName">case insensitive name</param>
        /// <param name="fixedName">output case sensitive name</param>
        /// <returns>true if a matching resource definition is found</returns>
        public static bool IsResource(string insensitiveName, out string fixedName)
        {
            var defs = PartResourceLibrary.Instance.resourceDefinitions;
            // PartResourceDefinitionList's array index accessor uses the resource id
            // instead of as a list index, so we need to use an enumerator.
            foreach (var def in defs)
            {
                // loop through definitions looking for a case insensitive name match,
                // return true if a match is found
                if (def.name.Equals(insensitiveName, StringComparison.OrdinalIgnoreCase))
                {
                    fixedName = def.name;
                    return true;
                }
            }
            fixedName = insensitiveName;
            return false;
        }

        /// <summary>
        /// Determines if a given case insensitive name corresponds with a defined resource, and
        /// outputs the resource id for easy access from Squad's lists.
        /// </summary>
        /// <param name="insensitiveName">case insensitive name</param>
        /// <param name="foundId">output id of the found resource, zero if none found</param>
        /// <returns>true if a matching resource definition is found</returns>
        public static bool IsResource(string insensitiveName, out int foundId)
        {
            var defs = PartResourceLibrary.Instance.resourceDefinitions;
            // PartResourceDefinitionList's array index accessor uses the resource id
            // instead of as a list index, so we need to use an enumerator.
            foreach (var def in defs)
            {
                // loop through definitions looking for a case insensitive name match,
                // return true if a match is found
                if (def.name.Equals(insensitiveName, StringComparison.OrdinalIgnoreCase))
                {
                    foundId = def.id;
                    return true;
                }
            }
            foundId = 0;
            return false;
        }

        /// <summary>
        /// Displays a popup dialog box with the given title, message, and single "OK" button.
        /// Use to provide simple information to the user that requires no direct input.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void DisplayPopupAlert(string title, string message, params string[] formatArgs)
        {
            PopupDialog.SpawnPopupDialog(
                new MultiOptionDialog(
                    "SPAWN_POPUP",
                    string.Format(message, formatArgs),
                    title,
                    HighLogic.UISkin,
                    new DialogGUIButton("OK", null, true)
                    ),
                true,
                HighLogic.UISkin
                );
        }
    }
}
