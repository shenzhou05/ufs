﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UFS
{
    public class EffectBase
    {
        /// <summary>
        /// 特效相对于载具的局部空间原点位置
        /// </summary>
        public Vector3 VesselOriginalPos { get; set; }

        /// <summary>
        /// 特效在载具的局部空间中的缩放大小
        /// </summary>
        public Vector3 VesselOriginalScale { get; set; }

        /// <summary>
        /// 特效附属于哪个载具
        /// </summary>
        public Vessel Vessel { get; set; }

        public EffectBase(Vessel vessel) : this(vessel, Vector3.zero, Vector3.one)
        {
        }

        public EffectBase(Vessel vessel, Vector3 vesselOrigialPos, Vector3 vesselOrigialScale)
        {
            this.Vessel = vessel ?? throw new NullReferenceException();
            this.VesselOriginalPos = vesselOrigialPos;
            this.VesselOriginalScale = vesselOrigialScale;

            List<float> collidersXMax = new List<float>();
            List<float> collidersXMin = new List<float>();
            List<float> collidersYMax = new List<float>();
            List<float> collidersYMin = new List<float>();
            List<float> collidersZMax = new List<float>();
            List<float> collidersZMin = new List<float>();
            List<Collider> colliderArray = new List<Collider>();

            foreach (var item in Vessel.Parts)
            {
                colliderArray.AddRange(item.GetComponentsInChildren<Collider>());
            }

            foreach (var item in colliderArray)
            {
                if (item.gameObject.name == "obj_girder_mesh" && item.gameObject.transform.parent.parent.parent.name == "launchclamp")
                    continue;
                if (item.gameObject.name == "obj_ground_mesh" && item.gameObject.transform.parent.parent.name == "launchclamp")
                    continue;
                if (item.gameObject.name == "base" && item.gameObject.transform.parent.name == "launchclamp")
                    continue;
                collidersXMax.Add(item.bounds.center.x + item.bounds.extents.x);
                collidersXMin.Add(item.bounds.center.x - item.bounds.extents.x);
                collidersYMax.Add(item.bounds.center.y + item.bounds.extents.y);
                collidersYMin.Add(item.bounds.center.y - item.bounds.extents.y);
                collidersZMax.Add(item.bounds.center.z + item.bounds.extents.z);
                collidersZMin.Add(item.bounds.center.z - item.bounds.extents.z);
            }

            collidersXMax.Sort();
            collidersXMin.Sort();
            collidersYMax.Sort();
            collidersYMin.Sort();
            collidersZMax.Sort();
            collidersZMin.Sort();

            if (collidersXMax.Count < 1)
            {
                this.effectWorldPosition = Vector3.zero;
                this.effectRadius = 0f;
            }
            else
            {
                Vector3 center = new Vector3(
                  (collidersXMax[collidersXMax.Count - 1] + collidersXMin[0]) / 2f,
                  (collidersYMax[collidersYMax.Count - 1] + collidersYMin[0]) / 2f,
                  (collidersZMax[collidersZMax.Count - 1] + collidersZMin[0]) / 2f);
                this.effectWorldPosition = center;
                Vector3 extents = new Vector3(collidersXMax[collidersXMax.Count - 1] - center.x, collidersYMax[collidersYMax.Count - 1] - center.y, collidersZMax[collidersZMax.Count - 1] - center.z);
                this.effectRadius = 2f * extents.magnitude;
            }
        }


        readonly protected Vector3 effectWorldPosition;
        /// <summary>
        /// 特效在世界空间中的位置（只适用于编辑器）
        /// </summary>
        public virtual Vector3 EffectWorldPosition
        {
            get
            {
                if (HighLogic.LoadedScene != GameScenes.EDITOR)
                    return Vector3.zero;
                return EffectWorldPosition + VesselOriginalPos;
            }
        }

        readonly protected float effectRadius;
        /// <summary>
        /// 特效在世界空间中的缩放（只适用于编辑器）
        /// </summary>
        public virtual Vector3 EffectWorldScale
        {
            get
            {
                if (HighLogic.LoadedScene != GameScenes.EDITOR)
                    return Vector3.zero;
                return effectRadius * VesselOriginalScale;
            }
        }
    }
}
