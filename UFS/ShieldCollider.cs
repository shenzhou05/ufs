﻿using BDArmory.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using BDArmory.Core;

namespace UFS
{
    [RequireComponent(typeof(Collider))]
    public class ShieldCollider : MonoBehaviour
    {
        public Part shieldPart;
        public bool startCheck = false;
        public float maxSpeed;
        public float maxMass;
        public float collisionDamage;
        public float explosionDamage = 100f;

        private double colliderCount = 0;
        private double perColliderCount = 0;
        private bool colliderDoOnce = true;
        private AudioSource audioSource;

        void FixedUpdate()
        {
            if (audioSource == null)
            {
                audioSource = this.gameObject.AddComponent<AudioSource>();
                audioSource.bypassListenerEffects = true;
                audioSource.minDistance = 100f;
                audioSource.maxDistance = 1000f;
                audioSource.priority = 1;
                audioSource.dopplerLevel = 0;
                audioSource.spatialBlend = 1;
                audioSource.loop = false;
                audioSource.time = 0;
                audioSource.volume = 1;
            }
            if (audioSource)
            {
                if (this.perColliderCount != colliderCount)
                {
                    //撞击
                    if (colliderDoOnce)
                    {
                        audioSource.clip = this.shieldPart.Modules.GetModule<ModuleShieldGenerator>().colliderShield;
                        audioSource.loop = true;
                        audioSource.Play();
                        colliderDoOnce = false;
                    }
                }
                else
                {
                    //不撞击
                    if (!colliderDoOnce)
                    {
                        colliderDoOnce = true;
                        audioSource.Stop();
                        audioSource.loop = false;
                        audioSource.clip = null;
                    }
                }
                this.perColliderCount = colliderCount;
                if (!this.shieldPart.Modules.GetModule<ModuleShieldGenerator>()._switchShield)
                {
                    colliderDoOnce = true;
                    audioSource.Stop();
                    audioSource.loop = false;
                    audioSource.clip = null;
                }
            }
        }

        void OnTriggerStay(Collider other)
        {
            if (startCheck)
            {
                Part IRpart = other.GetComponentInParent<Part>();
                if (IRpart && shieldPart)
                {
                    if (IRpart.vessel != this.shieldPart.vessel)
                    {
                        if (IRpart.Modules.GetModule<ModuleShieldGenerator>() != null)
                        {
                            colliderCount += 1;
                            shieldPart.Modules.GetModule<ModuleShieldGenerator>().AddDamage(Time.fixedDeltaTime * (float)Planetarium.TimeScale * collisionDamage, true);
                        }
                        else if (IRpart.vessel.GetTotalMass() > maxMass ||
                             (IRpart.vessel.srf_velocity - shieldPart.vessel.srf_velocity).magnitude > maxSpeed)
                        {
                            var speedVector = (IRpart.vessel.srf_velocity - shieldPart.vessel.srf_velocity).normalized;
                            var posVector = (shieldPart.vessel.GetWorldPos3D() - IRpart.vessel.GetWorldPos3D()).normalized;
                            if (Vector3.Dot(speedVector, posVector) > 0)
                            {
                                if (IRpart.Modules.GetModule<BDExplosivePart>() != null)
                                {
                                    //爆炸伤害
                                    var bed = IRpart.Modules.GetModule<BDExplosivePart>();
                                    bed.blastRadius = 0f;
                                    float tnt = bed.tntMass;
                                    bed.tntMass = 0f;
                                    shieldPart.Modules.GetModule<ModuleShieldGenerator>().AddDamage(tnt * explosionDamage);
                                    bed.part.explode();
                                }
                                else
                                {
                                    //动能伤害0.05*m(t)*v^2
                                    float kineticDamage = 0.05f * IRpart.mass * Mathf.Pow((float)(IRpart.vessel.srf_velocity - shieldPart.vessel.srf_velocity).magnitude, 2);
                                    shieldPart.Modules.GetModule<ModuleShieldGenerator>().AddDamage(kineticDamage);
                                   // IRpart.decouple(100);
                                    IRpart.explode();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
