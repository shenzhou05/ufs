﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UFS
{
    [KSPAddon(KSPAddon.Startup.FlightAndEditor, false)]
    public class BeautifyController : MonoBehaviour
    {
        private Beautify beautify;

        private void Start()
        {
            /*var pref = ResourcesController.test.LoadAsset("Panel") as GameObject;
            var panel = Instantiate(pref, Vector3.zero, Quaternion.identity) as GameObject;
            panel.transform.SetParent(MainCanvasUtil.MainCanvas.transform);
            panel.transform.localPosition = Vector3.zero;*/
            Beautify[] array = Object.FindObjectsOfType<Beautify>();
            foreach (var item in array)
                Destroy(item);
            GameObject mainCamera = Camera.main.gameObject;
            var tempBeautify = mainCamera.GetComponent<Beautify>();
            if (tempBeautify == null)
                tempBeautify = mainCamera.AddComponent<Beautify>();
            beautify = tempBeautify;
            if (beautify != Beautify.instance) return;
            Beautify.instance.enabled = true;

            Beautify.instance.lut = false;
            Beautify.instance.outline = false;
            Beautify.instance.nightVision = false;
            Beautify.instance.bloom = false;
            Beautify.instance.anamorphicFlares = false;
            Beautify.instance.lensDirt = false;
            Beautify.instance.vignetting = false;
            Beautify.instance.frame = false;

            Texture2D lutTexture = GameDatabase.Instance.GetTexture("UFS/Textures/lut", false);
            Beautify.instance.lutTexture = lutTexture;
            //Beautify.instance.lut = false;
            Texture2D dirt = GameDatabase.Instance.GetTexture("UFS/Textures/dirt", false);
            Beautify.instance.lensDirtTexture = dirt;
            /* Beautify.instance.lensDirt = true;
             Beautify.instance.vignetting = true;
             Beautify.instance.vignettingColor = new Color(0, 0, 0, 0.05f);
             Beautify.instance.anamorphicFlares = true;*/

            Beautify.instance.depthOfField = true;
            Beautify.instance.depthOfFieldAutofocus = true;
            Beautify.instance.lensDirt = true;
            Beautify.instance.lut = false;
            Beautify.instance.eyeAdaptation = true;
            Beautify.instance.anamorphicFlares = true;
        }
    }
}
