﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Scorpio;

namespace UFS
{
    [KSPAddon(KSPAddon.Startup.AllGameScenes, false)]
    public class ResourcesController : MonoBehaviour
    {
        public bool updata = true;
        private static AssetBundle m_ufsAB;
        public static AssetBundle ufsAB
        {
            private set
            {
                m_ufsAB = value;
            }
            get
            {
                string path = KSPUtil.ApplicationRootPath + "GameData/UFS/AssetBundles"; 
                if (m_ufsAB == null)
                    m_ufsAB = AssetBundle.LoadFromFile(path + "/ufs.ksp");
                return m_ufsAB;
            }
        }
        private static AssetBundle m_test;
        public static AssetBundle test
        {
            private set
            {
                m_test = value;
            }
            get
            {
                string path = KSPUtil.ApplicationRootPath + "GameData/UFS/AssetBundles";
                if (m_test== null)
                    m_test = AssetBundle.LoadFromFile(path + "/test.ksp");
                return m_test;
            }
        }
        private Script script = null;

        private void Start()
        {
            string path = KSPUtil.ApplicationRootPath + "GameData/UFS/AssetBundles";
            if (m_ufsAB == null)
            {
                m_ufsAB = AssetBundle.LoadFromFile(path + "/ufs.ksp");
            }
            if (m_test == null)
            {
                m_test = AssetBundle.LoadFromFile(path + "/test.ksp");
            }
        }

        private void Update()
        {
            if (updata)
            {
                string path = KSPUtil.ApplicationRootPath + "GameData/UFS/AssetBundles";
                if (m_ufsAB == null)
                {
                    m_ufsAB = AssetBundle.LoadFromFile(path + "/ufs.ksp");
                }
                if (m_test == null)
                {
                    m_test = AssetBundle.LoadFromFile(path + "/test.ksp");
                }
                //执行热更新
                if (Input.GetKeyDown(KeyCode.I))
                {
                    var value = new ScriptLaunch.Value[] { new ScriptLaunch.Value() { name = "baseObj", obj = this.gameObject } };
                    ScriptManager.GetInstance().Start(value);
                    script = ScriptManager.GetInstance().GetScript();
                }
            }
        }

        private void OnGUI()
        {
            Call("OnGUI");
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Call("OnRenderImage", source, destination);
        }

        private void Call(string func, params object[] args)
        {
            if (script != null && script.HasValue(func))
            {
                try
                {
                    ((ScriptFunction)script.GetValue(func)).call(args);
                }
                catch (Exception e)
                {
                    Debug.LogError(string.Format("ScriptComponent.Call is error func:{0}  {1}{2}", func, script.GetStackInfo(), e.ToString()));
                }
            }
        }
    }
}
