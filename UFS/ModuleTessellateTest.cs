﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UFS
{
    public class ModuleTessellateTest :PartModule
    {
        public override void OnStart(StartState state)
        {
            base.OnStart(state);
            var meshs = this.gameObject.transform.GetComponentsInChildren<MeshRenderer>();
            if (meshs != null)
            {
                var tessellationShader = ResourcesController.test.LoadAsset("TessellationShader") as Shader;
                foreach (var mesh in meshs)
                {
                    Material pre = mesh.material;
                    if (pre == null) return;
                    Texture2D mainTex = pre.GetTexture("_MainTex") as Texture2D;
                    Texture2D bumpTex = pre.GetTexture("_BumpMap") as Texture2D;
                    var tessMat = new Material(tessellationShader);
                    if (mainTex != null)
                    {
                        tessMat.SetTexture("_MainTex", mainTex);
                        tessMat.SetTexture("_DispTex", mainTex);
                        if (bumpTex != null)
                            tessMat.SetTexture("_NormalMap", bumpTex);
                        mesh.material = tessMat;
                    }
                }
            }
        }
    }
}
