﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UFS
{
    public class BeautifySInfo
    {
        public string name = "";
        // public string path = "";
        public List<SCShaderPass> passes = new List<SCShaderPass>();
        public List<SCKeyword> keywords = new List<SCKeyword>();
        public bool pendingChanges;
        public bool editedByShaderControl;
        public int enabledKeywordCount;

        public void Add(SCShaderPass pass)
        {
            passes.Add(pass);
            UpdateKeywords();
        }

        public void EnableKeywords()
        {
            keywords.ForEach((SCKeyword keyword) => keyword.enabled = true);
        }

        void UpdateKeywords()
        {
            passes.ForEach((SCShaderPass pass) =>
            {
                for (int l = 0; l < pass.keywordLines.Count; l++)
                {
                    SCKeywordLine line = pass.keywordLines[l];
                    for (int k = 0; k < line.keywords.Count; k++)
                    {
                        SCKeyword keyword = line.keywords[k];
                        if (!keywords.Contains(keyword))
                        {
                            if (SCKeywordChecker.IsValid(keyword.name))
                            {
                                keywords.Add(keyword);
                            }
                        }
                    }
                }
            });

            enabledKeywordCount = 0;
            keywords.ForEach((SCKeyword kw) =>
            {
                if (kw.enabled)
                    enabledKeywordCount++;
            });
        }

        public SCKeyword GetKeyword(string name)
        {
            int kCount = keywords.Count;
            for (int k = 0; k < kCount; k++)
            {
                SCKeyword keyword = keywords[k];
                if (keyword.name.Equals(name))
                    return keyword;
            }
            return new SCKeyword(name);
        }
    }

    public class SCShaderPass
    {
        public int pass;
        public List<SCKeywordLine> keywordLines = new List<SCKeywordLine>();
        public int keywordCount;

        public void Add(SCKeywordLine keywordLine)
        {
            keywordLines.Add(keywordLine);
            UpdateKeywordCount();
        }

        public void Add(List<SCKeywordLine> keywordLines)
        {
            this.keywordLines.AddRange(keywordLines);
            UpdateKeywordCount();
        }

        void UpdateKeywordCount()
        {
            keywordCount = 0;
            keywordLines.ForEach((SCKeywordLine obj) => keywordCount += obj.keywordCount);
        }

        public void Clear()
        {
            keywordCount = 0;
            keywordLines.Clear();
        }
    }

    public class SCKeywordLine
    {
        public List<SCKeyword> keywords = new List<SCKeyword>();
        public bool hasUnderscoreVariant;

        public SCKeyword GetKeyword(string name)
        {
            int kc = keywords.Count;
            for (int k = 0; k < kc; k++)
            {
                SCKeyword keyword = keywords[k];
                if (keyword.name.Equals(name))
                {
                    return keyword;
                }
            }
            return null;
        }

        public void Add(SCKeyword keyword)
        {
            if (GetKeyword(keyword.name) != null)
                return;
            // ignore underscore keywords
            bool goodKeyword = false;
            for (int k = 0; k < keyword.name.Length; k++)
            {
                if (keyword.name[k] != '_')
                {
                    goodKeyword = true;
                    break;
                }
            }
            if (goodKeyword)
            {
                keywords.Add(keyword);
            }
            else
            {
                keyword.isUnderscoreKeyword = true;
                hasUnderscoreVariant = true;
            }
        }

        public void DisableKeywords()
        {
            keywords.ForEach((SCKeyword obj) => obj.enabled = false);
        }

        public void Clear()
        {
            keywords.Clear();
        }

        public int keywordCount
        {
            get
            {
                return keywords.Count;
            }
        }

        public int keywordsEnabledCount
        {
            get
            {
                int kCount = keywords.Count;
                int enabledCount = 0;
                for (int k = 0; k < kCount; k++)
                {
                    if (keywords[k].enabled)
                        enabledCount++;
                }
                return enabledCount;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < keywords.Count; k++)
            {
                if (k > 0)
                    sb.Append(" ");
                sb.Append(keywords[k].name);
            }
            return sb.ToString();
        }

    }

    public class SCKeyword
    {
        public string name;
        public bool enabled;
        public bool isUnderscoreKeyword;

        public SCKeyword(string name)
        {
            this.name = name;
            enabled = true;
        }

        public override bool Equals(object obj)
        {
            if (obj is SCKeyword)
            {
                SCKeyword other = (SCKeyword)obj;
                return name.Equals(other.name);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public override string ToString()
        {
            return name;
        }
    }

    static class SCKeywordChecker
    {

        static Dictionary<string, string> dict = new Dictionary<string, string>();

        static SCKeywordChecker()
        {
            string[][] kw = new string[][] {
                                                                new string[] { Beautify.SKW_DALTONIZE, "Daltonize" },
                                                                new string[] { Beautify.SKW_LUT, "LUT" },
                                                                new string[] { Beautify.SKW_NIGHT_VISION, "Night Vision" },
                                                                new string[] { Beautify.SKW_THERMAL_VISION, "Thermal Vision" },
                                                                new string[] {
                                                                                Beautify.SKW_DEPTH_OF_FIELD,
                                                                                "Depth of Field (DoF)"
                                                                },
                                                                new string[] {
                                                                                Beautify.SKW_DEPTH_OF_FIELD_TRANSPARENT,
                                                                                "DoF Transp. & Exclusion"
                                                                },
                                                                new string[] { Beautify.SKW_OUTLINE, "Outline" },
                                                                new string[] { Beautify.SKW_DIRT, "Lens Dirt" },
                                                                new string[] { Beautify.SKW_BLOOM, "Bloom & Flares" },
                                                                new string[] { Beautify.SKW_TONEMAP_ACES, "ACES Tonemap" },
                                                                new string[] { Beautify.SKW_EYE_ADAPTATION, "Eye Adaptation" },
                                                                new string[] { Beautify.SKW_PURKINJE, "Purkinje" },
                                                                new string[] { Beautify.SKW_VIGNETTING, "Vignetting" },
                                                                new string[] {
                                                                                Beautify.SKW_VIGNETTING_MASK,
                                                                                "Vignetting Mask"
                                                                },
                                                                new string[] { Beautify.SKW_FRAME, "Frame" },
                                                                new string[] { Beautify.SKW_FRAME_MASK, "Frame Mask" }
                                                };
            for (int k = 0; k < kw.Length; k++)
                dict[kw[k][0]] = kw[k][1];
        }

        public static bool IsValid(string name)
        {
            return dict.ContainsKey(name);
        }

        public static string Translate(string name)
        {
            if (dict.ContainsKey(name))
            {
                return dict[name];
            }
            else
            {
                return name;
            }
        }
    }

    public enum BEAUTIFY_QUALITY
    {
        BestQuality
    }

    public enum BEAUTIFY_PRESET
    {
        Soft = 10,
        Medium = 20,
        Strong = 30,
        Exaggerated = 40,
        Custom = 999
    }

    public enum BEAUTIFY_TMO
    {
        Linear = 0,
        ACES = 10
    }


    [RequireComponent(typeof(Camera))]
    public class Beautify : MonoBehaviour
    {


        #region General settings

        [SerializeField]
        BEAUTIFY_PRESET
                        _preset = BEAUTIFY_PRESET.Medium;

        public BEAUTIFY_PRESET preset
        {
            get { return _preset; }
            set
            {
                if (_preset != value)
                {
                    _preset = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        BEAUTIFY_QUALITY
                        _quality = BEAUTIFY_QUALITY.BestQuality;

        public BEAUTIFY_QUALITY quality
        {
            get { return _quality; }
            set
            {
                if (_quality != value)
                {
                    _quality = value;
                    UpdateQualitySettings();
                    UpdateMaterialProperties();
                }
            }
        }

        /* [SerializeField]
         BeautifyProfile _profile;

         public BeautifyProfile profile
         {
             get { return _profile; }
             set
             {
                 if (_profile != value)
                 {
                     _profile = value;
                     if (_profile != null)
                     {
                         _profile.Load(this);
                         _preset = BEAUTIFY_PRESET.Custom;
                     }
                 }
             }
         }*/

        #endregion

        #region RGB Dither

        [SerializeField]
        [Range(0, 0.2f)]
        float
                        _dither = 0.02f;

        public float dither
        {
            get { return _dither; }
            set
            {
                if (_dither != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _dither = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0, 1f)]
        float
                        _ditherDepth = 0f;

        public float ditherDepth
        {
            get { return _ditherDepth; }
            set
            {
                if (_ditherDepth != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _ditherDepth = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Sharpen Settings


        [SerializeField]
        [Range(0, 1f)]
        float
                        _sharpenMinDepth = 0f;

        public float sharpenMinDepth
        {
            get { return _sharpenMinDepth; }
            set
            {
                if (_sharpenMinDepth != value)
                {
                    _sharpenMinDepth = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0, 1.1f)]
        float
                        _sharpenMaxDepth = 0.999f;

        public float sharpenMaxDepth
        {
            get { return _sharpenMaxDepth; }
            set
            {
                if (_sharpenMaxDepth != value)
                {
                    _sharpenMaxDepth = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 15f)]
        float
                        _sharpen = 2f;

        public float sharpen
        {
            get { return _sharpen; }
            set
            {
                if (_sharpen != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _sharpen = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 0.05f)]
        float
                        _sharpenDepthThreshold = 0.035f;

        public float sharpenDepthThreshold
        {
            get { return _sharpenDepthThreshold; }
            set
            {
                if (_sharpenDepthThreshold != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _sharpenDepthThreshold = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Color
                        _tintColor = new Color(1, 1, 1, 0);

        public Color tintColor
        {
            get { return _tintColor; }
            set
            {
                if (_tintColor != value)
                {
                    _tintColor = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 0.2f)]
        float
                        _sharpenRelaxation = 0.08f;

        public float sharpenRelaxation
        {
            get { return _sharpenRelaxation; }
            set
            {
                if (_sharpenRelaxation != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _sharpenRelaxation = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0, 1f)]
        float
                        _sharpenClamp = 0.45f;

        public float sharpenClamp
        {
            get { return _sharpenClamp; }
            set
            {
                if (_sharpenClamp != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _sharpenClamp = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0, 1f)]
        float
                        _sharpenMotionSensibility = 0.5f;

        public float sharpenMotionSensibility
        {
            get { return _sharpenMotionSensibility; }
            set
            {
                if (_sharpenMotionSensibility != value)
                {
                    _sharpenMotionSensibility = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Color grading

        [SerializeField]
        [Range(-2f, 3f)]
        float
                        _saturate = 1f;

        public float saturate
        {
            get { return _saturate; }
            set
            {
                if (_saturate != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _saturate = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0.5f, 1.5f)]
        float
                        _contrast = 1.02f;

        public float contrast
        {
            get { return _contrast; }
            set
            {
                if (_contrast != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _contrast = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 2f)]
        float
                        _brightness = 1.05f;

        public float brightness
        {
            get { return _brightness; }
            set
            {
                if (_brightness != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _brightness = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 2f)]
        float
                        _daltonize = 0f;

        public float daltonize
        {
            get { return _daltonize; }
            set
            {
                if (_daltonize != value)
                {
                    _preset = BEAUTIFY_PRESET.Custom;
                    _daltonize = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Vignetting

        [SerializeField]
        bool
                        _vignetting = false;

        public bool vignetting
        {
            get { return _vignetting; }
            set
            {
                if (_vignetting != value)
                {
                    _vignetting = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Color
                        _vignettingColor = new Color(0.3f, 0.3f, 0.3f, 0.05f);

        public Color vignettingColor
        {
            get { return _vignettingColor; }
            set
            {
                if (_vignettingColor != value)
                {
                    _vignettingColor = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0, 1)]
        float
        _vignettingFade = 0;

        public float vignettingFade
        {
            get { return _vignettingFade; }
            set
            {
                if (_vignettingFade != value)
                {
                    _vignettingFade = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _vignettingCircularShape = false;

        public bool vignettingCircularShape
        {
            get { return _vignettingCircularShape; }
            set
            {
                if (_vignettingCircularShape != value)
                {
                    _vignettingCircularShape = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        float
                        _vignettingAspectRatio = 1.0f;

        public float vignettingAspectRatio
        {
            get { return _vignettingAspectRatio; }
            set
            {
                if (_vignettingAspectRatio != value)
                {
                    _vignettingAspectRatio = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField, Range(0, 1f)]
        float
                        _vignettingBlink = 0f;

        public float vignettingBlink
        {
            get { return _vignettingBlink; }
            set
            {
                if (_vignettingBlink != value)
                {
                    _vignettingBlink = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        Texture2D
                        _vignettingMask;

        public Texture2D vignettingMask
        {
            get { return _vignettingMask; }
            set
            {
                if (_vignettingMask != value)
                {
                    _vignettingMask = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Frame

        [SerializeField]
        bool
                        _frame = false;

        public bool frame
        {
            get { return _frame; }
            set
            {
                if (_frame != value)
                {
                    _frame = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Color
                        _frameColor = new Color(1, 1, 1, 0.047f);

        public Color frameColor
        {
            get { return _frameColor; }
            set
            {
                if (_frameColor != value)
                {
                    _frameColor = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Texture2D
                        _frameMask;

        public Texture2D frameMask
        {
            get { return _frameMask; }
            set
            {
                if (_frameMask != value)
                {
                    _frameMask = value;
                    UpdateMaterialProperties();
                }
            }
        }


        #endregion

        #region LUT

        [SerializeField]
        bool
                        _lut = false;

        public bool lut
        {
            get { return _lut; }
            set
            {
                if (_lut != value)
                {
                    _lut = value;
                    if (_lut)
                    {
                        _nightVision = false;
                        _thermalVision = false;
                    }
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _lutIntensity = 1f;

        public float lutIntensity
        {
            get { return _lutIntensity; }
            set
            {
                if (_lutIntensity != value)
                {
                    _lutIntensity = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Texture2D _lutTexture;

        public Texture2D lutTexture
        {
            get { return _lutTexture; }
            set
            {
                if (_lutTexture != value)
                {
                    _lutTexture = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Night Vision

        [SerializeField]
        bool
                        _nightVision = false;

        public bool nightVision
        {
            get { return _nightVision; }
            set
            {
                if (_nightVision != value)
                {
                    _nightVision = value;
                    if (_nightVision)
                    {
                        _thermalVision = false;
                        _lut = false;
                        _vignetting = true;
                        _vignettingFade = 0;
                        _vignettingColor = new Color(0, 0, 0, 32f / 255f);
                        _vignettingCircularShape = true;
                    }
                    else
                    {
                        _vignetting = false;
                    }
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Color
                        _nightVisionColor = new Color(0.5f, 1f, 0.5f, 0.5f);

        public Color nightVisionColor
        {
            get { return _nightVisionColor; }
            set
            {
                if (_nightVisionColor != value)
                {
                    _nightVisionColor = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Outline

        [SerializeField]
        bool
                        _outline = false;

        public bool outline
        {
            get { return _outline; }
            set
            {
                if (_outline != value)
                {
                    _outline = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Color
                        _outlineColor = new Color(0, 0, 0, 0.8f);

        public Color outlineColor
        {
            get { return _outlineColor; }
            set
            {
                if (_outlineColor != value)
                {
                    _outlineColor = value;
                    UpdateMaterialProperties();
                }
            }
        }


        #endregion

        #region Thermal Vision

        [SerializeField]
        bool
                        _thermalVision = false;

        public bool thermalVision
        {
            get { return _thermalVision; }
            set
            {
                if (_thermalVision != value)
                {
                    _thermalVision = value;
                    if (_thermalVision)
                    {
                        _nightVision = false;
                        _lut = false;
                        _vignetting = true;
                        _vignettingFade = 0;
                        _vignettingColor = new Color(1f, 16f / 255f, 16f / 255f, 18f / 255f);
                        _vignettingCircularShape = true;
                    }
                    else
                    {
                        _vignetting = false;
                    }
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Lens Dirt

        [SerializeField]
        bool
                        _lensDirt = false;

        public bool lensDirt
        {
            get { return _lensDirt; }
            set
            {
                if (_lensDirt != value)
                {
                    _lensDirt = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _lensDirtThreshold = 0.5f;

        public float lensDirtThreshold
        {
            get { return _lensDirtThreshold; }
            set
            {
                if (_lensDirtThreshold != value)
                {
                    _lensDirtThreshold = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _lensDirtIntensity = 0.9f;

        public float lensDirtIntensity
        {
            get { return _lensDirtIntensity; }
            set
            {
                if (_lensDirtIntensity != value)
                {
                    _lensDirtIntensity = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Texture2D
                        _lensDirtTexture;

        public Texture2D lensDirtTexture
        {
            get { return _lensDirtTexture; }
            set
            {
                if (_lensDirtTexture != value)
                {
                    _lensDirtTexture = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Bloom

        [SerializeField]
        bool
                        _bloom = false;

        public bool bloom
        {
            get { return _bloom; }
            set
            {
                if (_bloom != value)
                {
                    _bloom = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        LayerMask
                        _bloomCullingMask = 0;

        public LayerMask bloomCullingMask
        {
            get { return _bloomCullingMask; }
            set
            {
                if (_bloomCullingMask != value)
                {
                    _bloomCullingMask = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(1f, 4f)]
        float
                        _bloomLayerMaskDownsampling = 1f;

        public float bloomLayerMaskDownsampling
        {
            get { return _bloomLayerMaskDownsampling; }
            set
            {
                if (_bloomLayerMaskDownsampling != value)
                {
                    _bloomLayerMaskDownsampling = Mathf.Max(value, 1f);
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        [Range(0, 10f)]
        float
                        _bloomIntensity = 1f;

        public float bloomIntensity
        {
            get { return _bloomIntensity; }
            set
            {
                if (_bloomIntensity != value)
                {
                    _bloomIntensity = Mathf.Abs(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        float
                        _bloomMaxBrightness = 1000f;

        public float bloomMaxBrightness
        {
            get { return _bloomMaxBrightness; }
            set
            {
                if (_bloomMaxBrightness != value)
                {
                    _bloomMaxBrightness = Mathf.Abs(value);
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        [Range(0f, 3f)]
        float
                        _bloomBoost0 = 0f;

        public float bloomBoost0
        {
            get { return _bloomBoost0; }
            set
            {
                if (_bloomBoost0 != value)
                {
                    _bloomBoost0 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 3f)]
        float
                        _bloomBoost1 = 0f;

        public float bloomBoost1
        {
            get { return _bloomBoost1; }
            set
            {
                if (_bloomBoost1 != value)
                {
                    _bloomBoost1 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 3f)]
        float
                        _bloomBoost2 = 0f;

        public float bloomBoost2
        {
            get { return _bloomBoost2; }
            set
            {
                if (_bloomBoost2 != value)
                {
                    _bloomBoost2 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 3f)]
        float
                        _bloomBoost3 = 0f;

        public float bloomBoost3
        {
            get { return _bloomBoost3; }
            set
            {
                if (_bloomBoost3 != value)
                {
                    _bloomBoost3 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 3f)]
        float
                        _bloomBoost4 = 0f;

        public float bloomBoost4
        {
            get { return _bloomBoost4; }
            set
            {
                if (_bloomBoost4 != value)
                {
                    _bloomBoost4 = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        [Range(0f, 3f)]
        float
                        _bloomBoost5 = 0f;

        public float bloomBoost5
        {
            get { return _bloomBoost5; }
            set
            {
                if (_bloomBoost5 != value)
                {
                    _bloomBoost5 = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        bool
                        _bloomAntiflicker = false;

        public bool bloomAntiflicker
        {
            get { return _bloomAntiflicker; }
            set
            {
                if (_bloomAntiflicker != value)
                {
                    _bloomAntiflicker = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _bloomUltra = false;

        public bool bloomUltra
        {
            get { return _bloomUltra; }
            set
            {
                if (_bloomUltra != value)
                {
                    _bloomUltra = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 5f)]
        float
                        _bloomThreshold = 0.75f;

        public float bloomThreshold
        {
            get { return _bloomThreshold; }
            set
            {
                if (_bloomThreshold != value)
                {
                    _bloomThreshold = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _bloomCustomize = false;

        public bool bloomCustomize
        {
            get { return _bloomCustomize; }
            set
            {
                if (_bloomCustomize != value)
                {
                    _bloomCustomize = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _bloomDebug = false;

        public bool bloomDebug
        {
            get { return _bloomDebug; }
            set
            {
                if (_bloomDebug != value)
                {
                    _bloomDebug = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomWeight0 = 0.5f;

        public float bloomWeight0
        {
            get { return _bloomWeight0; }
            set
            {
                if (_bloomWeight0 != value)
                {
                    _bloomWeight0 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomWeight1 = 0.5f;

        public float bloomWeight1
        {
            get { return _bloomWeight1; }
            set
            {
                if (_bloomWeight1 != value)
                {
                    _bloomWeight1 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomWeight2 = 0.5f;

        public float bloomWeight2
        {
            get { return _bloomWeight2; }
            set
            {
                if (_bloomWeight2 != value)
                {
                    _bloomWeight2 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomWeight3 = 0.5f;

        public float bloomWeight3
        {
            get { return _bloomWeight3; }
            set
            {
                if (_bloomWeight3 != value)
                {
                    _bloomWeight3 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomWeight4 = 0.5f;

        public float bloomWeight4
        {
            get { return _bloomWeight4; }
            set
            {
                if (_bloomWeight4 != value)
                {
                    _bloomWeight4 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomWeight5 = 0.5f;

        public float bloomWeight5
        {
            get { return _bloomWeight5; }
            set
            {
                if (_bloomWeight5 != value)
                {
                    _bloomWeight5 = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _bloomBlur = true;

        public bool bloomBlur
        {
            get { return _bloomBlur; }
            set
            {
                if (_bloomBlur != value)
                {
                    _bloomBlur = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _bloomDepthAtten = 0;

        public float bloomDepthAtten
        {
            get { return _bloomDepthAtten; }
            set
            {
                if (_bloomDepthAtten != value)
                {
                    _bloomDepthAtten = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        [Range(-1f, 1f)]
        float
                        _bloomLayerZBias = 0;

        public float bloomLayerZBias
        {
            get { return _bloomLayerZBias; }
            set
            {
                if (_bloomLayerZBias != value)
                {
                    _bloomLayerZBias = Mathf.Clamp(value, -1f, 1f);
                    UpdateMaterialProperties();
                }
            }
        }



        #endregion

        #region Anamorphic Flares


        [SerializeField]
        bool
                        _anamorphicFlares = false;

        public bool anamorphicFlares
        {
            get { return _anamorphicFlares; }
            set
            {
                if (_anamorphicFlares != value)
                {
                    _anamorphicFlares = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 10f)]
        float
                        _anamorphicFlaresIntensity = 1f;

        public float anamorphicFlaresIntensity
        {
            get { return _anamorphicFlaresIntensity; }
            set
            {
                if (_anamorphicFlaresIntensity != value)
                {
                    _anamorphicFlaresIntensity = Mathf.Abs(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _anamorphicFlaresAntiflicker = false;

        public bool anamorphicFlaresAntiflicker
        {
            get { return _anamorphicFlaresAntiflicker; }
            set
            {
                if (_anamorphicFlaresAntiflicker != value)
                {
                    _anamorphicFlaresAntiflicker = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        bool
                        _anamorphicFlaresUltra = false;

        public bool anamorphicFlaresUltra
        {
            get { return _anamorphicFlaresUltra; }
            set
            {
                if (_anamorphicFlaresUltra != value)
                {
                    _anamorphicFlaresUltra = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 5f)]
        float
                        _anamorphicFlaresThreshold = 0.75f;

        public float anamorphicFlaresThreshold
        {
            get { return _anamorphicFlaresThreshold; }
            set
            {
                if (_anamorphicFlaresThreshold != value)
                {
                    _anamorphicFlaresThreshold = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0.1f, 2f)]
        float
                        _anamorphicFlaresSpread = 1f;

        public float anamorphicFlaresSpread
        {
            get { return _anamorphicFlaresSpread; }
            set
            {
                if (_anamorphicFlaresSpread != value)
                {
                    _anamorphicFlaresSpread = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _anamorphicFlaresVertical = false;

        public bool anamorphicFlaresVertical
        {
            get { return _anamorphicFlaresVertical; }
            set
            {
                if (_anamorphicFlaresVertical != value)
                {
                    _anamorphicFlaresVertical = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Color
                        _anamorphicFlaresTint = new Color(0.5f, 0.5f, 1f, 0f);

        public Color anamorphicFlaresTint
        {
            get { return _anamorphicFlaresTint; }
            set
            {
                if (_anamorphicFlaresTint != value)
                {
                    _anamorphicFlaresTint = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        bool
        _anamorphicFlaresBlur = true;

        public bool anamorphicFlaresBlur
        {
            get { return _anamorphicFlaresBlur; }
            set
            {
                if (_anamorphicFlaresBlur != value)
                {
                    _anamorphicFlaresBlur = value;
                    UpdateMaterialProperties();
                }
            }
        }


        #endregion

        #region Depth of Field


        [SerializeField]
        bool
                        _depthOfField = false;

        public bool depthOfField
        {
            get { return _depthOfField; }
            set
            {
                if (_depthOfField != value)
                {
                    _depthOfField = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _depthOfFieldTransparencySupport = false;

        public bool depthOfFieldTransparencySupport
        {
            get { return _depthOfFieldTransparencySupport; }
            set
            {
                if (_depthOfFieldTransparencySupport != value)
                {
                    _depthOfFieldTransparencySupport = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        Transform
                        _depthOfFieldTargetFocus;

        public Transform depthOfFieldTargetFocus
        {
            get { return _depthOfFieldTargetFocus; }
            set
            {
                if (_depthOfFieldTargetFocus != value)
                {
                    _depthOfFieldTargetFocus = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _depthOfFieldDebug = false;

        public bool depthOfFieldDebug
        {
            get { return _depthOfFieldDebug; }
            set
            {
                if (_depthOfFieldDebug != value)
                {
                    _depthOfFieldDebug = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _depthOfFieldAutofocus = false;

        public bool depthOfFieldAutofocus
        {
            get { return _depthOfFieldAutofocus; }
            set
            {
                if (_depthOfFieldAutofocus != value)
                {
                    _depthOfFieldAutofocus = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        float
                        _depthOfFieldAutofocusMinDistance = 0;

        public float depthOfFieldAutofocusMinDistance
        {
            get { return _depthOfFieldAutofocusMinDistance; }
            set
            {
                if (_depthOfFieldAutofocusMinDistance != value)
                {
                    _depthOfFieldAutofocusMinDistance = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        float
                        _depthOfFieldAutofocusMaxDistance = 10000;

        public float depthOfFieldAutofocusMaxDistance
        {
            get { return _depthOfFieldAutofocusMaxDistance; }
            set
            {
                if (_depthOfFieldAutofocusMaxDistance != value)
                {
                    _depthOfFieldAutofocusMaxDistance = value;
                    UpdateMaterialProperties();
                }
            }
        }



        [SerializeField]
        LayerMask
                        _depthOfFieldAutofocusLayerMask = -1;

        public LayerMask depthOfFieldAutofocusLayerMask
        {
            get { return _depthOfFieldAutofocusLayerMask; }
            set
            {
                if (_depthOfFieldAutofocusLayerMask != value)
                {
                    _depthOfFieldAutofocusLayerMask = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        LayerMask
                        _depthOfFieldExclusionLayerMask = 0;

        public LayerMask depthOfFieldExclusionLayerMask
        {
            get { return _depthOfFieldExclusionLayerMask; }
            set
            {
                if (_depthOfFieldExclusionLayerMask != value)
                {
                    _depthOfFieldExclusionLayerMask = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        [Range(1, 4)]
        float
                        _depthOfFieldExclusionLayerMaskDownsampling = 1f;

        public float depthOfFieldExclusionLayerMaskDownsampling
        {
            get { return _depthOfFieldExclusionLayerMaskDownsampling; }
            set
            {
                if (_depthOfFieldExclusionLayerMaskDownsampling != value)
                {
                    _depthOfFieldExclusionLayerMaskDownsampling = Mathf.Max(value, 1f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(1, 4)]
        float
                        _depthOfFieldTransparencySupportDownsampling = 1f;

        public float depthOfFieldTransparencySupportDownsampling
        {
            get { return _depthOfFieldTransparencySupportDownsampling; }
            set
            {
                if (_depthOfFieldTransparencySupportDownsampling != value)
                {
                    _depthOfFieldTransparencySupportDownsampling = Mathf.Max(value, 1f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0.9f, 1f)]
        float
                        _depthOfFieldExclusionBias = 0.99f;

        public float depthOfFieldExclusionBias
        {
            get { return _depthOfFieldExclusionBias; }
            set
            {
                if (_depthOfFieldExclusionBias != value)
                {
                    _depthOfFieldExclusionBias = Mathf.Clamp01(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(1f, 100f)]
        float
                        _depthOfFieldDistance = 1f;

        public float depthOfFieldDistance
        {
            get { return _depthOfFieldDistance; }
            set
            {
                if (_depthOfFieldDistance != value)
                {
                    _depthOfFieldDistance = Mathf.Max(value, 1f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0.001f, 5f)]
        float
                        _depthOfFieldFocusSpeed = 1f;

        public float depthOfFieldFocusSpeed
        {
            get { return _depthOfFieldFocusSpeed; }
            set
            {
                if (_depthOfFieldFocusSpeed != value)
                {
                    _depthOfFieldFocusSpeed = Mathf.Clamp(value, 0.001f, 1f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(1, 5)]
        int
                        _depthOfFieldDownsampling = 2;

        public int depthOfFieldDownsampling
        {
            get { return _depthOfFieldDownsampling; }
            set
            {
                if (_depthOfFieldDownsampling != value)
                {
                    _depthOfFieldDownsampling = Mathf.Max(value, 1);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(2, 16)]
        int
                        _depthOfFieldMaxSamples = 4;

        public int depthOfFieldMaxSamples
        {
            get { return _depthOfFieldMaxSamples; }
            set
            {
                if (_depthOfFieldMaxSamples != value)
                {
                    _depthOfFieldMaxSamples = Mathf.Max(value, 2);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0.005f, 0.5f)]
        float
                        _depthOfFieldFocalLength = 0.050f;

        public float depthOfFieldFocalLength
        {
            get { return _depthOfFieldFocalLength; }
            set
            {
                if (_depthOfFieldFocalLength != value)
                {
                    _depthOfFieldFocalLength = Mathf.Abs(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        float
                        _depthOfFieldAperture = 2.8f;

        public float depthOfFieldAperture
        {
            get { return _depthOfFieldAperture; }
            set
            {
                if (_depthOfFieldAperture != value)
                {
                    _depthOfFieldAperture = Mathf.Abs(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _depthOfFieldForegroundBlur = true;

        public bool depthOfFieldForegroundBlur
        {
            get { return _depthOfFieldForegroundBlur; }
            set
            {
                if (_depthOfFieldForegroundBlur != value)
                {
                    _depthOfFieldForegroundBlur = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _depthOfFieldForegroundBlurHQ;

        public bool depthOfFieldForegroundBlurHQ
        {
            get { return _depthOfFieldForegroundBlurHQ; }
            set
            {
                if (_depthOfFieldForegroundBlurHQ != value)
                {
                    _depthOfFieldForegroundBlurHQ = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        float
                        _depthOfFieldForegroundDistance = 0.25f;

        public float depthOfFieldForegroundDistance
        {
            get { return _depthOfFieldForegroundDistance; }
            set
            {
                if (_depthOfFieldForegroundDistance != value)
                {
                    _depthOfFieldForegroundDistance = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        bool
                        _depthOfFieldBokeh = true;

        public bool depthOfFieldBokeh
        {
            get { return _depthOfFieldBokeh; }
            set
            {
                if (_depthOfFieldBokeh != value)
                {
                    _depthOfFieldBokeh = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0.5f, 3f)]
        float
                        _depthOfFieldBokehThreshold = 1f;

        public float depthOfFieldBokehThreshold
        {
            get { return _depthOfFieldBokehThreshold; }
            set
            {
                if (_depthOfFieldBokehThreshold != value)
                {
                    _depthOfFieldBokehThreshold = Mathf.Max(value, 0f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 8f)]
        float
                        _depthOfFieldBokehIntensity = 2f;

        public float depthOfFieldBokehIntensity
        {
            get { return _depthOfFieldBokehIntensity; }
            set
            {
                if (_depthOfFieldBokehIntensity != value)
                {
                    _depthOfFieldBokehIntensity = Mathf.Max(value, 0);
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        float
                        _depthOfFieldMaxBrightness = 1000f;

        public float depthOfFieldMaxBrightness
        {
            get { return _depthOfFieldMaxBrightness; }
            set
            {
                if (_depthOfFieldMaxBrightness != value)
                {
                    _depthOfFieldMaxBrightness = Mathf.Abs(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        FilterMode _depthOfFieldFilterMode = FilterMode.Bilinear;

        public FilterMode depthOfFieldFilterMode
        {
            get { return _depthOfFieldFilterMode; }
            set
            {
                if (_depthOfFieldFilterMode != value)
                {
                    _depthOfFieldFilterMode = value;
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Eye Adaptation


        [SerializeField]
        bool
                        _eyeAdaptation = false;

        public bool eyeAdaptation
        {
            get { return _eyeAdaptation; }
            set
            {
                if (_eyeAdaptation != value)
                {
                    _eyeAdaptation = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _eyeAdaptationMinExposure = 0.2f;

        public float eyeAdaptationMinExposure
        {
            get { return _eyeAdaptationMinExposure; }
            set
            {
                if (_eyeAdaptationMinExposure != value)
                {
                    _eyeAdaptationMinExposure = Mathf.Clamp01(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(1f, 100f)]
        float
                        _eyeAdaptationMaxExposure = 5f;

        public float eyeAdaptationMaxExposure
        {
            get { return _eyeAdaptationMaxExposure; }
            set
            {
                if (_eyeAdaptationMaxExposure != value)
                {
                    _eyeAdaptationMaxExposure = Mathf.Clamp(value, 1f, 100f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _eyeAdaptationSpeedToLight = 0.4f;

        public float eyeAdaptationSpeedToLight
        {
            get { return _eyeAdaptationSpeedToLight; }
            set
            {
                if (_eyeAdaptationSpeedToLight != value)
                {
                    _eyeAdaptationSpeedToLight = Mathf.Clamp01(value);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _eyeAdaptationSpeedToDark = 0.2f;

        public float eyeAdaptationSpeedToDark
        {
            get { return _eyeAdaptationSpeedToDark; }
            set
            {
                if (_eyeAdaptationSpeedToDark != value)
                {
                    _eyeAdaptationSpeedToDark = Mathf.Clamp01(value);
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Purkinje effect

        [SerializeField]
        bool
                        _purkinje = false;

        public bool purkinje
        {
            get { return _purkinje; }
            set
            {
                if (_purkinje != value)
                {
                    _purkinje = value;
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 5f)]
        float
                        _purkinjeAmount = 1f;

        public float purkinjeAmount
        {
            get { return _purkinjeAmount; }
            set
            {
                if (_purkinjeAmount != value)
                {
                    _purkinjeAmount = Mathf.Clamp(value, 0f, 5f);
                    UpdateMaterialProperties();
                }
            }
        }

        [SerializeField]
        [Range(0f, 1f)]
        float
                        _purkinjeLuminanceThreshold = 0.15f;

        public float purkinjeLuminanceThreshold
        {
            get { return _purkinjeLuminanceThreshold; }
            set
            {
                if (purkinjeLuminanceThreshold != value)
                {
                    _purkinjeLuminanceThreshold = Mathf.Clamp(value, 0f, 1f);
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion

        #region Tonemapping


        [SerializeField]
        BEAUTIFY_TMO
                        _tonemap = BEAUTIFY_TMO.Linear;

        public BEAUTIFY_TMO tonemap
        {
            get { return _tonemap; }
            set
            {
                if (_tonemap != value)
                {
                    _tonemap = value;
                    if (_tonemap == BEAUTIFY_TMO.ACES)
                    {
                        _saturate = 0;
                        _contrast = 1f;
                    }
                    UpdateMaterialProperties();
                }
            }
        }

        #endregion





        #region Blur


        [SerializeField]
        bool
                        _blur = false;

        public bool blur
        {
            get { return _blur; }
            set
            {
                if (_blur != value)
                {
                    _blur = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        [Range(0, 4f)]
        float
                        _blurIntensity = 1f;

        public float blurIntensity
        {
            get { return _blurIntensity; }
            set
            {
                if (_blurIntensity != value)
                {
                    _blurIntensity = value;
                    UpdateMaterialProperties();
                }
            }
        }


        #endregion


        #region Pixelate

        [SerializeField]
        [Range(1, 256)]
        int
                        _pixelateAmount = 1;

        public int pixelateAmount
        {
            get { return _pixelateAmount; }
            set
            {
                if (_pixelateAmount != value)
                {
                    _pixelateAmount = value;
                    UpdateMaterialProperties();
                }
            }
        }


        [SerializeField]
        bool
                        _pixelateDownscale = false;

        public bool pixelateDownscale
        {
            get { return _pixelateDownscale; }
            set
            {
                if (_pixelateDownscale != value)
                {
                    _pixelateDownscale = value;
                    UpdateMaterialProperties();
                }
            }
        }



        #endregion

        public static Beautify instance
        {
            get
            {
                if (_beautify == null)
                {
                    foreach (Camera camera in Camera.allCameras)
                    {
                        _beautify = camera.GetComponent<Beautify>();
                        if (_beautify != null)
                            break;
                    }
                }
                return _beautify;
            }
        }

        public Camera cameraEffect { get { return currentCamera; } }

        // Internal stuff **************************************************************************************************************

        public bool isDirty;
        static Beautify _beautify;

        // Shader keywords
        public const string SKW_BLOOM = "BEAUTIFY_BLOOM";
        public const string SKW_LUT = "BEAUTIFY_LUT";
        public const string SKW_NIGHT_VISION = "BEAUTIFY_NIGHT_VISION";
        public const string SKW_THERMAL_VISION = "BEAUTIFY_THERMAL_VISION";
        public const string SKW_OUTLINE = "BEAUTIFY_OUTLINE";
        public const string SKW_FRAME = "BEAUTIFY_FRAME";
        public const string SKW_FRAME_MASK = "BEAUTIFY_FRAME_MASK";
        public const string SKW_DALTONIZE = "BEAUTIFY_DALTONIZE";
        public const string SKW_DIRT = "BEAUTIFY_DIRT";
        public const string SKW_VIGNETTING = "BEAUTIFY_VIGNETTING";
        public const string SKW_VIGNETTING_MASK = "BEAUTIFY_VIGNETTING_MASK";
        public const string SKW_DEPTH_OF_FIELD = "BEAUTIFY_DEPTH_OF_FIELD";
        public const string SKW_DEPTH_OF_FIELD_TRANSPARENT = "BEAUTIFY_DEPTH_OF_FIELD_TRANSPARENT";
        public const string SKW_EYE_ADAPTATION = "BEAUTIFY_EYE_ADAPTATION";
        public const string SKW_TONEMAP_ACES = "BEAUTIFY_TONEMAP_ACES";
        public const string SKW_PURKINJE = "BEAUTIFY_PURKINJE";
        public const string SKW_BLOOM_USE_DEPTH = "BEAUTIFY_BLOOM_USE_DEPTH";
        public const string SKW_BLOOM_USE_LAYER = "BEAUTIFY_BLOOM_USE_LAYER";

        Material bMatDesktop, bMatMobile, bMatBasic;

        [SerializeField]
        Material bMat;
        Camera currentCamera;
        Vector3 camPrevForward, camPrevPos;
        float currSens;
        int renderPass;
        RenderTextureFormat rtFormat;
        RenderTexture[] rt, rtAF, rtEA;
        RenderTexture rtEAacum, rtEAHist;
        float dofPrevDistance, dofLastAutofocusDistance;
        Vector4 dofLastBokehData;
        Camera sceneCamera, depthCam;
        GameObject depthCamObj;
        List<string> shaderKeywords;
        Shader depthShader, dofExclusionShader;
        bool shouldUpdateMaterialProperties;
        const string BEAUTIFY_BUILD_HINT = "BeautifyBuildHint62RC1";
        Texture2D flareNoise;
        RenderTexture dofDepthTexture, dofExclusionTexture, bloomSourceTexture, bloomSourceDepthTexture, pixelateTexture;
        RenderTextureDescriptor rtDescBase;
        int dofCurrentLayerMaskValue;

        #region Game loop events

        // Creates a private material used to the effect
        void OnEnable()
        {
            currentCamera = GetComponent<Camera>();
            /*  if (_profile != null)
              {
                  _profile.Load(this);
              }*/
            UpdateMaterialPropertiesNow();
        }

#if UNITY_5_4
								void Start () {
												// Fix prefab glitch on Unity 5.4
												enabled = false;
												enabled = true;
								}
#endif

        void OnDestroy()
        {
            CleanUpRT();
            if (depthCamObj != null)
            {
                DestroyImmediate(depthCamObj);
                depthCamObj = null;
            }
            if (rtEAacum != null)
                rtEAacum.Release();
            if (rtEAHist != null)
                rtEAHist.Release();
            if (bMatDesktop != null)
            {
                DestroyImmediate(bMatDesktop);
                bMatDesktop = null;
            }
            if (bMatMobile != null)
            {
                DestroyImmediate(bMatMobile);
                bMatMobile = null;
            }
            if (bMatBasic != null)
            {
                DestroyImmediate(bMatBasic);
                bMatBasic = null;
            }
            bMat = null;
        }

        void Reset()
        {
            UpdateMaterialPropertiesNow();
        }

        void LateUpdate()
        {
            if (bMat == null || !Application.isPlaying || _sharpenMotionSensibility <= 0)
                return;
            // Motion sensibility 
            float angleDiff = Vector3.Angle(camPrevForward, currentCamera.transform.forward) * _sharpenMotionSensibility;
            float posDiff = (currentCamera.transform.position - camPrevPos).sqrMagnitude * 10f * _sharpenMotionSensibility;

            float diff = angleDiff + posDiff;
            if (diff > 0.1f)
            {
                camPrevForward = currentCamera.transform.forward;
                camPrevPos = currentCamera.transform.position;
                if (diff > _sharpenMotionSensibility)
                    diff = _sharpenMotionSensibility;
                currSens += diff;
                float min = _sharpen * _sharpenMotionSensibility * 0.75f;
                float max = _sharpen * (1f + _sharpenMotionSensibility) * 0.5f;
                currSens = Mathf.Clamp(currSens, min, max);
            }
            else
            {
                if (currSens <= 0.001f)
                    return;
                currSens *= 0.75f;
            }
            float tempSharpen = Mathf.Clamp(_sharpen - currSens, 0, _sharpen);
            UpdateSharpenParams(tempSharpen);
        }

        void OnPreCull()
        {   // Aquas issue with OnPreRender
            bool bloomLayerMaskUsed = (_bloom || _anamorphicFlares) && _bloomCullingMask != 0;
            if (!enabled || !gameObject.activeSelf || currentCamera == null || bMat == null || (!_depthOfField && !bloomLayerMaskUsed))
                return;

            CleanUpRT();

            if (dofCurrentLayerMaskValue != _depthOfFieldExclusionLayerMask.value)
                shouldUpdateMaterialProperties = true;

            if (depthOfField && (_depthOfFieldTransparencySupport || _depthOfFieldExclusionLayerMask != 0))
            {
                CheckDoFTransparencySupport();
                CheckDoFExclusionMask();
            }
            if (bloomLayerMaskUsed)
            {
                CheckBloomCullingLayer();
            }
        }

        void OnPreRender()
        {

            UpdateMaterialProperties();
            if (_pixelateDownscale && _pixelateAmount > 1 && rtDescBase.width > 1 && rtDescBase.height > 1)
            {
                RenderTextureDescriptor rtPixDesc = rtDescBase;
                rtPixDesc.width = Mathf.RoundToInt(Mathf.Max(1, currentCamera.pixelWidth / _pixelateAmount));
                float aspectRatio = (float)currentCamera.pixelHeight / currentCamera.pixelWidth;
                rtPixDesc.height = Mathf.Max(1, Mathf.RoundToInt(rtPixDesc.width * aspectRatio));
                pixelateTexture = RenderTexture.GetTemporary(rtPixDesc);
                currentCamera.targetTexture = pixelateTexture;
            }

        }

        void CleanUpRT()
        {
            if (dofDepthTexture != null)
            {
                RenderTexture.ReleaseTemporary(dofDepthTexture);
                dofDepthTexture = null;
            }
            if (dofExclusionTexture != null)
            {
                RenderTexture.ReleaseTemporary(dofExclusionTexture);
                dofExclusionTexture = null;
            }
            if (bloomSourceTexture != null)
            {
                RenderTexture.ReleaseTemporary(bloomSourceTexture);
                bloomSourceTexture = null;
            }
            if (bloomSourceDepthTexture != null)
            {
                RenderTexture.ReleaseTemporary(bloomSourceDepthTexture);
                bloomSourceDepthTexture = null;
            }
            if (pixelateTexture != null)
            {
                RenderTexture.ReleaseTemporary(pixelateTexture);
                pixelateTexture = null;
            }
        }

        void CheckDoFTransparencySupport()
        {
            if (depthCam == null)
            {
                if (depthCamObj == null)
                {
                    depthCamObj = new GameObject("DepthCamera");
                    depthCamObj.hideFlags = HideFlags.HideAndDontSave;
                    depthCam = depthCamObj.AddComponent<Camera>();
                    depthCam.enabled = false;
                }
                else
                {
                    depthCam = depthCamObj.GetComponent<Camera>();
                    if (depthCam == null)
                    {
                        DestroyImmediate(depthCamObj);
                        depthCamObj = null;
                        return;
                    }
                }
            }
            depthCam.CopyFrom(currentCamera);
            depthCam.depthTextureMode = DepthTextureMode.None;
            depthCam.renderingPath = RenderingPath.Forward;
            float downsampling = _depthOfFieldTransparencySupportDownsampling * _depthOfFieldDownsampling;
            dofDepthTexture = RenderTexture.GetTemporary((int)(currentCamera.pixelWidth / downsampling), (int)(currentCamera.pixelHeight / downsampling), 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            dofDepthTexture.filterMode = FilterMode.Point;
            depthCam.backgroundColor = new Color(0.9882353f, 0.4470558f, 0.75f, 0f); // new Color (1, 1, 1, 1);
            depthCam.clearFlags = CameraClearFlags.SolidColor;
            depthCam.targetTexture = dofDepthTexture;
            depthCam.cullingMask = ~_depthOfFieldExclusionLayerMask;
            if (depthShader == null)
            {
                depthShader = ResourcesController.test.LoadAsset("CopyDepth") as Shader; 
            }
            depthCam.RenderWithShader(depthShader, "RenderType");
            bMat.SetTexture("_DepthTexture", dofDepthTexture);
        }

        void CheckDoFExclusionMask()
        {
            if (depthCam == null)
            {
                if (depthCamObj == null)
                {
                    depthCamObj = new GameObject("DepthCamera");
                    depthCamObj.hideFlags = HideFlags.HideAndDontSave;
                    depthCam = depthCamObj.AddComponent<Camera>();
                    depthCam.enabled = false;
                }
                else
                {
                    depthCam = depthCamObj.GetComponent<Camera>();
                    if (depthCam == null)
                    {
                        DestroyImmediate(depthCamObj);
                        depthCamObj = null;
                        return;
                    }
                }
            }
            depthCam.CopyFrom(currentCamera);
            depthCam.depthTextureMode = DepthTextureMode.None;
            depthCam.renderingPath = RenderingPath.Forward;
            float downsampling = _depthOfFieldExclusionLayerMaskDownsampling * _depthOfFieldDownsampling;
            dofExclusionTexture = RenderTexture.GetTemporary((int)(currentCamera.pixelWidth / downsampling), (int)(currentCamera.pixelHeight / downsampling), 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            dofExclusionTexture.filterMode = FilterMode.Point;
            depthCam.backgroundColor = new Color(0.9882353f, 0.4470558f, 0.75f, 0f); // new Color (1, 1, 1, 1);
            depthCam.clearFlags = CameraClearFlags.SolidColor;
            depthCam.targetTexture = dofExclusionTexture;
            depthCam.cullingMask = _depthOfFieldExclusionLayerMask;
            if (dofExclusionShader == null)
            {
                dofExclusionShader = ResourcesController.test.LoadAsset("CopyDepthBiased") as Shader;
            }
            depthCam.RenderWithShader(dofExclusionShader, null);
            bMat.SetTexture("_DofExclusionTexture", dofExclusionTexture);
        }

        void CheckBloomCullingLayer()
        {
            // Reuses depth camera
            if (depthCam == null)
            {
                if (depthCamObj == null)
                {
                    depthCamObj = new GameObject("DepthCamera");
                    depthCamObj.hideFlags = HideFlags.HideAndDontSave;
                    depthCam = depthCamObj.AddComponent<Camera>();
                    depthCam.enabled = false;
                }
                else
                {
                    depthCam = depthCamObj.GetComponent<Camera>();
                    if (depthCam == null)
                    {
                        DestroyImmediate(depthCamObj);
                        depthCamObj = null;
                        return;
                    }
                }
            }
            depthCam.CopyFrom(currentCamera);
            depthCam.depthTextureMode = DepthTextureMode.None;
            depthCam.allowMSAA = false;
            depthCam.allowHDR = false;
            int size;

            size = _bloomUltra ? (int)(currentCamera.pixelHeight / 4) * 4 : 512;
            size = (int)(size * (1f / _bloomLayerMaskDownsampling) / 4) * 4;

            float aspectRatio = (float)currentCamera.pixelHeight / currentCamera.pixelWidth;
            bloomSourceTexture = RenderTexture.GetTemporary(size, Mathf.Max(1, (int)(size * aspectRatio)), 0, rtFormat);
            bloomSourceDepthTexture = RenderTexture.GetTemporary(bloomSourceTexture.width, bloomSourceTexture.height, 24, RenderTextureFormat.Depth);
            depthCam.clearFlags = CameraClearFlags.SolidColor;
#if UNITY_5_4_OR_NEWER
            depthCam.stereoTargetEye = StereoTargetEyeMask.None;
#endif
            depthCam.renderingPath = RenderingPath.Forward; // currently this feature does not work in deferred
            depthCam.backgroundColor = Color.black;
            depthCam.SetTargetBuffers(bloomSourceTexture.colorBuffer, bloomSourceDepthTexture.depthBuffer);
            depthCam.cullingMask = _bloomCullingMask;
            depthCam.Render();
            bMat.SetTexture("_BloomSourceTex", bloomSourceTexture);
            bMat.SetTexture("_BloomSourceDepth", bloomSourceDepthTexture);
        }

        protected virtual void OnRenderImage(RenderTexture source, RenderTexture destination)
        {

            if (bMat == null || !enabled)
            {
                Graphics.Blit(source, destination);
                return;
            }

            if (shouldUpdateMaterialProperties)
            {
                UpdateMaterialPropertiesNow();
            }

            bool allowExtraEffects = true;

            // Copy source settings; RRTT will be created using descriptor to take advantage of the vrUsage field.
            rtDescBase = source.descriptor;
            rtDescBase.msaaSamples = 1;
            rtDescBase.colorFormat = rtFormat;
            rtDescBase.depthBufferBits = 0;

            // Prepare compare & final blur buffer
            RenderTexture rtBeauty = null;
            RenderTexture rtBlurTex = null;
            RenderTexture rtCustomBloom = null;

            float aspectRatio = (float)source.height / source.width;

            bool doFinalBlur = _blur && _blurIntensity > 0 && allowExtraEffects;
            if (renderPass == 0 || doFinalBlur)
            {
                if (doFinalBlur)
                {
                    int size;
                    if (_blurIntensity < 1f)
                    {
                        size = (int)Mathf.Lerp(currentCamera.pixelWidth, 512, _blurIntensity);
                    }
                    else
                    {
                        size = _quality == BEAUTIFY_QUALITY.BestQuality ? 512 : 256;
                        size = (int)(size / _blurIntensity);
                    }
                    //																				float aspectRatio = (float)currentCamera.pixelHeight / currentCamera.pixelWidth;
                    RenderTextureDescriptor rtBlurDesc = rtDescBase;
                    rtBlurDesc.width = size;
                    rtBlurDesc.height = Mathf.Max(1, (int)(size * aspectRatio));
                    rtBlurTex = RenderTexture.GetTemporary(rtBlurDesc);
                    if (renderPass == 0)
                    {
                        rtBeauty = RenderTexture.GetTemporary(rtBlurDesc);
                    }
                }
                else
                {
                    rtBeauty = RenderTexture.GetTemporary(rtDescBase); //source.descriptor);
                }
            }

            RenderTexture rtPixelated = null;
            RenderTexture rtDoF = null;

            if (allowExtraEffects)
            {
                // Pixelate
                if (_pixelateAmount > 1)
                {
                    source.filterMode = FilterMode.Point;
                    if (!_pixelateDownscale)
                    {
                        RenderTextureDescriptor rtPixDesc = rtDescBase;
                        rtPixDesc.width = Mathf.RoundToInt(Mathf.Max(1, source.width / _pixelateAmount));
                        rtPixDesc.height = Mathf.Max(1, Mathf.RoundToInt(rtPixDesc.width * aspectRatio));
                        rtPixelated = RenderTexture.GetTemporary(rtPixDesc); //rtPixDesc.width, rtPixDesc.height, -1);
                        rtPixelated.filterMode = FilterMode.Point;
                        Graphics.Blit(source, rtPixelated, bMat, 22);
                        source = rtPixelated;
                    }
                }


                // DoF!
                if (_depthOfField)
                {
#if UNITY_EDITOR
                    if (sceneCamera == null && Camera.current != null && Camera.current.name.Equals("SceneCamera"))
                    {
                        sceneCamera = Camera.current;
                    }

                    if (Camera.current != sceneCamera)
                    {
                        if (!bMat.IsKeywordEnabled(SKW_DEPTH_OF_FIELD))
                        {
                            bMat.EnableKeyword(SKW_DEPTH_OF_FIELD);
                        }
                        if ((_depthOfFieldTransparencySupport || _depthOfFieldExclusionLayerMask != 0) && !bMat.IsKeywordEnabled(SKW_DEPTH_OF_FIELD_TRANSPARENT))
                        {
                            bMat.EnableKeyword(SKW_DEPTH_OF_FIELD_TRANSPARENT);
                        }
#endif
                    UpdateDepthOfFieldData();

                    int pass = _quality == BEAUTIFY_QUALITY.BestQuality ? 12 : 6;
                    RenderTextureDescriptor rtDofDescriptor = rtDescBase;
                    rtDofDescriptor.width = source.width / _depthOfFieldDownsampling;
                    rtDofDescriptor.height = source.height / _depthOfFieldDownsampling;
                    rtDoF = RenderTexture.GetTemporary(rtDofDescriptor);
                    rtDoF.filterMode = _depthOfFieldFilterMode;
                    Graphics.Blit(source, rtDoF, bMat, pass);

                    if (_quality == BEAUTIFY_QUALITY.BestQuality)
                    {
                        pass = _depthOfFieldBokeh ? 14 : 19;
                    }
                    else
                    {
                        pass = _depthOfFieldBokeh ? 8 : 15;

                    }
                    if (_quality == BEAUTIFY_QUALITY.BestQuality && _depthOfFieldForegroundBlur && _depthOfFieldForegroundBlurHQ)
                    {
                        BlurThisAlpha(rtDoF, 16);
                    }
                    BlurThisDoF(rtDoF, pass);

                    if (_depthOfFieldDebug)
                    {
                        source.MarkRestoreExpected();
                        pass = _quality == BEAUTIFY_QUALITY.BestQuality ? 13 : 7;
                        Graphics.Blit(rtDoF, destination, bMat, pass);
                        RenderTexture.ReleaseTemporary(rtDoF);
                        return;
                    }

                    bMat.SetTexture("_DoFTex", rtDoF);
#if UNITY_EDITOR
                    }
                    else
                    {
                        // Cancels DoF
                        if (bMat.IsKeywordEnabled(SKW_DEPTH_OF_FIELD))
                        {
                            bMat.DisableKeyword(SKW_DEPTH_OF_FIELD); // .SetVector ("_BokehData", new Vector4 (10000, 0, 0, 0));
                        }
                        if (bMat.IsKeywordEnabled(SKW_DEPTH_OF_FIELD_TRANSPARENT))
                        {
                            bMat.DisableKeyword(SKW_DEPTH_OF_FIELD_TRANSPARENT); // .SetVector ("_BokehData", new Vector4 (10000, 0, 0, 0));
                        }
                    }
#endif
                }
            }

            if (allowExtraEffects && (_lensDirt || _bloom || _anamorphicFlares))
            {
                RenderTexture rtBloom = null;

                int PYRAMID_COUNT, size;
                PYRAMID_COUNT = 5;
                size = _bloomUltra ? (int)(source.height / 4) * 4 : 512;


                // Bloom buffers
                if (rt == null || rt.Length != PYRAMID_COUNT + 1)
                {
                    rt = new RenderTexture[PYRAMID_COUNT + 1];
                }
                // Anamorphic flare buffers
                if (rtAF == null || rtAF.Length != PYRAMID_COUNT + 1)
                {
                    rtAF = new RenderTexture[PYRAMID_COUNT + 1];
                }

                //bool useBloomSourceTexture = (_bloom || _anamorphicFlares) && _bloomCullingMask != 0 && bloomSourceTexture;
                if (_bloom || (_lensDirt && !_anamorphicFlares))
                {
                    UpdateMaterialBloomIntensityAndThreshold();
                    RenderTextureDescriptor rtBloomDescriptor = rtDescBase;
                    for (int k = 0; k <= PYRAMID_COUNT; k++)
                    {
                        rtBloomDescriptor.width = size;
                        rtBloomDescriptor.height = Mathf.Max(1, (int)(size * aspectRatio));
                        rt[k] = RenderTexture.GetTemporary(rtBloomDescriptor);
                        size /= 2;
                    }
                    rtBloom = rt[0];

                    if (_quality == BEAUTIFY_QUALITY.BestQuality && _bloomAntiflicker)
                    {
                        Graphics.Blit(source, rt[0], bMat, 9);
                    }
                    else
                    {
                        Graphics.Blit(source, rt[0], bMat, 2);
                    }

                    BlurThis(rt[0]);

                    for (int k = 0; k < PYRAMID_COUNT; k++)
                    {
                        Graphics.Blit(rt[k], rt[k + 1], bMat, 7);
                        BlurThis(rt[k + 1]);
                    }

                    if (_bloom)
                    {
                        for (int k = PYRAMID_COUNT; k > 0; k--)
                        {
                            rt[k - 1].MarkRestoreExpected();
                            Graphics.Blit(rt[k], rt[k - 1], bMat, _quality == BEAUTIFY_QUALITY.BestQuality ? 8 : 13);
                        }
                        if (quality == BEAUTIFY_QUALITY.BestQuality && _bloomCustomize)
                        {
                            bMat.SetTexture("_BloomTex4", rt[4]);
                            bMat.SetTexture("_BloomTex3", rt[3]);
                            bMat.SetTexture("_BloomTex2", rt[2]);
                            bMat.SetTexture("_BloomTex1", rt[1]);
                            bMat.SetTexture("_BloomTex", rt[0]);
                            RenderTextureDescriptor rtCustomBloomDescriptor = rt[0].descriptor;
                            rtCustomBloom = RenderTexture.GetTemporary(rtCustomBloomDescriptor); // rt[0].width, rt[0].height, 0, rtFormat);
                            rtBloom = rtCustomBloom;
                            Graphics.Blit(rt[PYRAMID_COUNT], rtBloom, bMat, 6);
                        }
                    }

                }

                // anamorphic flares
                if (_anamorphicFlares)
                {
                    UpdateMaterialAnamorphicIntensityAndThreshold();

                    int sizeAF;

                    sizeAF = _anamorphicFlaresUltra ? (int)(source.height / 4) * 4 : 512;


                    RenderTextureDescriptor rtAFDescriptor = rtDescBase;
                    for (int origSize = sizeAF, k = 0; k <= PYRAMID_COUNT; k++)
                    {
                        if (_anamorphicFlaresVertical)
                        {
                            rtAFDescriptor.width = origSize;
                            rtAFDescriptor.height = Mathf.Max(1, (int)(sizeAF * aspectRatio / _anamorphicFlaresSpread));
                            rtAF[k] = RenderTexture.GetTemporary(rtAFDescriptor);
                        }
                        else
                        {
                            rtAFDescriptor.width = Mathf.Max(1, (int)(sizeAF * aspectRatio / _anamorphicFlaresSpread));
                            rtAFDescriptor.height = origSize;
                            rtAF[k] = RenderTexture.GetTemporary(rtAFDescriptor);
                        }
                        sizeAF /= 2;
                    }

                    if (_anamorphicFlaresAntiflicker && _quality == BEAUTIFY_QUALITY.BestQuality)
                    {
                        //Graphics.Blit(useBloomSourceTexture ? bloomSourceTexture : source, rtAF[0], bMat, 9);
                        Graphics.Blit(source, rtAF[0], bMat, 9);
                    }
                    else
                    {
                        //Graphics.Blit (useBloomSourceTexture ? bloomSourceTexture : source, rtAF [0], bMat, 2);
                        Graphics.Blit(source, rtAF[0], bMat, 2);
                    }

                    rtAF[0] = BlurThisOneDirection(rtAF[0], _anamorphicFlaresVertical);

                    for (int k = 0; k < PYRAMID_COUNT; k++)
                    {
                        Graphics.Blit(rtAF[k], rtAF[k + 1], bMat, 7);
                        rtAF[k + 1] = BlurThisOneDirection(rtAF[k + 1], _anamorphicFlaresVertical);
                    }

                    for (int k = PYRAMID_COUNT; k > 0; k--)
                    {
                        rtAF[k - 1].MarkRestoreExpected();
                        if (k == 1)
                        {
                            Graphics.Blit(rtAF[k], rtAF[k - 1], bMat, _quality == BEAUTIFY_QUALITY.BestQuality ? 10 : 14); // applies intensity in last stage
                        }
                        else
                        {
                            Graphics.Blit(rtAF[k], rtAF[k - 1], bMat, _quality == BEAUTIFY_QUALITY.BestQuality ? 8 : 13);
                        }
                    }
                    if (_bloom)
                    {
                        if (_lensDirt)
                        {
                            rt[3].MarkRestoreExpected();
                            Graphics.Blit(rtAF[3], rt[3], bMat, _quality == BEAUTIFY_QUALITY.BestQuality ? 11 : 13);
                        }
                        rtBloom.MarkRestoreExpected();
                        Graphics.Blit(rtAF[0], rtBloom, bMat, _quality == BEAUTIFY_QUALITY.BestQuality ? 11 : 13);
                    }
                    else
                    {
                        rtBloom = rtAF[0];
                    }
                    UpdateMaterialBloomIntensityAndThreshold();
                }


                if (rtBloom != null)
                {
                    bMat.SetTexture("_BloomTex", rtBloom);
                }
                else
                {
                    if (bMat.IsKeywordEnabled(SKW_BLOOM))
                    {
                        bMat.DisableKeyword(SKW_BLOOM); // required to avoid Metal issue
                    }
                    bMat.SetVector("_Bloom", Vector4.zero);
                }

                if (_lensDirt)
                {
                    bMat.SetTexture("_ScreenLum", (_anamorphicFlares && !_bloom) ? rtAF[3] : rt[3]);
                }

            }

            if (_lensDirt)
            {
                Vector4 dirtData = new Vector4(1.0f, 1.0f / (1.01f - _lensDirtIntensity), _lensDirtThreshold, Mathf.Max(_bloomIntensity, 1f));
                bMat.SetVector("_Dirt", dirtData);
            }

            // tonemap + eye adaptation + purkinje
            bool requiresLuminanceComputation = Application.isPlaying && allowExtraEffects && (_eyeAdaptation || _purkinje);
            if (requiresLuminanceComputation)
            {
                int rtEACount = _quality == BEAUTIFY_QUALITY.BestQuality ? 9 : 8;
                int sizeEA = (int)Mathf.Pow(2, rtEACount);
                if (rtEA == null || rtEA.Length < rtEACount)
                    rtEA = new RenderTexture[rtEACount];
                RenderTextureDescriptor rtLumDescriptor = rtDescBase;
                for (int k = 0; k < rtEACount; k++)
                {
                    rtLumDescriptor.width = sizeEA;
                    rtLumDescriptor.height = sizeEA;
                    rtEA[k] = RenderTexture.GetTemporary(rtLumDescriptor);
                    sizeEA /= 2;
                }
                Graphics.Blit(source, rtEA[0], bMat, _quality == BEAUTIFY_QUALITY.BestQuality ? 22 : 18);
                int lumRT = rtEACount - 1;
                int basePass = _quality == BEAUTIFY_QUALITY.BestQuality ? 15 : 9;
                for (int k = 0; k < lumRT; k++)
                {
                    Graphics.Blit(rtEA[k], rtEA[k + 1], bMat, k == 0 ? basePass : basePass + 1);
                }
                bMat.SetTexture("_EALumSrc", rtEA[lumRT]);
                if (rtEAacum == null)
                {
                    int rawCopyPass = _quality == BEAUTIFY_QUALITY.BestQuality ? 22 : 18;
                    RenderTextureDescriptor rtEASmallDesc = rtDescBase;
                    rtEASmallDesc.width = 2;
                    rtEASmallDesc.height = 2;
                    rtEAacum = new RenderTexture(rtEASmallDesc);
                    Graphics.Blit(rtEA[lumRT], rtEAacum, bMat, rawCopyPass);
                    rtEAHist = new RenderTexture(rtEASmallDesc);
                    Graphics.Blit(rtEAacum, rtEAHist, bMat, rawCopyPass);
                }
                else
                {
                    rtEAacum.MarkRestoreExpected();
                    Graphics.Blit(rtEA[lumRT], rtEAacum, bMat, basePass + 2);
                    Graphics.Blit(rtEAacum, rtEAHist, bMat, basePass + 3);
                }
                bMat.SetTexture("_EAHist", rtEAHist);
                bMat.SetTexture("_EALum", rtEAacum);
            }

            // Final Pass
            if (rtBeauty != null)
            {
                Graphics.Blit(source, rtBeauty, bMat, 1);
                bMat.SetTexture("_CompareTex", rtBeauty);
            }
            if (rtBlurTex != null)
            {
                float blurScale = _blurIntensity > 1f ? 1f : _blurIntensity;
                if (rtBeauty != null)
                {
                    Graphics.Blit(rtBeauty, rtBlurTex, bMat, renderPass);
                    BlurThis(rtBlurTex, blurScale);
                }
                else
                {
                    BlurThisDownscaling(source, rtBlurTex, blurScale);
                }
                BlurThis(rtBlurTex, blurScale);
                if (_quality == BEAUTIFY_QUALITY.BestQuality)
                {
                    BlurThis(rtBlurTex, blurScale);
                }
                if (rtBeauty != null)
                {
                    bMat.SetTexture("_CompareTex", rtBlurTex);
                    Graphics.Blit(source, destination, bMat, renderPass);
                }
                else
                {
                    Graphics.Blit(rtBlurTex, destination, bMat, renderPass);
                }
                RenderTexture.ReleaseTemporary(rtBlurTex);
            }
            else
            {
                Graphics.Blit(source, destination, bMat, renderPass);
            }

            // Release RTs used in final pass
            if (rtEA != null)
            {
                for (int k = 0; k < rtEA.Length; k++)
                {
                    if (rtEA[k] != null)
                    {
                        RenderTexture.ReleaseTemporary(rtEA[k]);
                        rtEA[k] = null;
                    }
                }
            }
            if (rt != null)
            {
                for (int k = 0; k < rt.Length; k++)
                {
                    if (rt[k] != null)
                    {
                        RenderTexture.ReleaseTemporary(rt[k]);
                        rt[k] = null;
                    }
                }
            }
            if (rtAF != null)
            {
                for (int k = 0; k < rtAF.Length; k++)
                {
                    if (rtAF[k] != null)
                    {
                        RenderTexture.ReleaseTemporary(rtAF[k]);
                        rtAF[k] = null;
                    }
                }
            }
            if (rtCustomBloom != null)
            {
                RenderTexture.ReleaseTemporary(rtCustomBloom);
            }
            if (rtDoF != null)
            {
                RenderTexture.ReleaseTemporary(rtDoF);
            }
            if (rtBeauty != null)
            {
                RenderTexture.ReleaseTemporary(rtBeauty);
            }
            if (rtPixelated != null)
            {
                RenderTexture.ReleaseTemporary(rtPixelated);
            }

        }

        void OnPostRender()
        {
            if (_pixelateDownscale && _pixelateAmount > 1 && pixelateTexture != null)
            {
                RenderTexture.active = null;
                currentCamera.targetTexture = null;
                pixelateTexture.filterMode = FilterMode.Point;
                Graphics.Blit(pixelateTexture, (RenderTexture)null);
            }
        }


        void BlurThis(RenderTexture rt, float blurScale = 1f)
        {
            RenderTextureDescriptor desc = rt.descriptor;
            RenderTexture rt2 = RenderTexture.GetTemporary(desc);
            rt2.filterMode = FilterMode.Bilinear;
            bMat.SetFloat("_BlurScale", blurScale);
            Graphics.Blit(rt, rt2, bMat, 4);
#if !UNITY_STANDALONE && (!UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS || UNITY_WII || UNITY_PS4 || UNITY_XBOXONE || UNITY_TIZEN || UNITY_TVOS || UNITY_WSA || UNITY_WSA_10_0 || UNITY_WEBGL)
            rt.DiscardContents();
#endif
            Graphics.Blit(rt2, rt, bMat, 5);
            RenderTexture.ReleaseTemporary(rt2);
        }

        void BlurThisDownscaling(RenderTexture rt, RenderTexture downscaled, float blurScale = 1f)
        {
            RenderTextureDescriptor desc = rt.descriptor;
            desc.width = downscaled.width;
            desc.height = downscaled.height;
            RenderTexture rt2 = RenderTexture.GetTemporary(desc);
            rt2.filterMode = FilterMode.Bilinear;
            float ratio = rt.width / desc.width;
            bMat.SetFloat("_BlurScale", blurScale * ratio);
            Graphics.Blit(rt, rt2, bMat, 4);
            bMat.SetFloat("_BlurScale", blurScale);
#if !UNITY_STANDALONE && (!UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS || UNITY_WII || UNITY_PS4 || UNITY_XBOXONE || UNITY_TIZEN || UNITY_TVOS || UNITY_WSA || UNITY_WSA_10_0 || UNITY_WEBGL)
            downscaled.DiscardContents();
#endif
            Graphics.Blit(rt2, downscaled, bMat, 5);
            RenderTexture.ReleaseTemporary(rt2);
        }

        RenderTexture BlurThisOneDirection(RenderTexture rt, bool vertical, float blurScale = 1f)
        {
            RenderTextureDescriptor desc = rt.descriptor;
            RenderTexture rt2 = RenderTexture.GetTemporary(desc);
            rt2.filterMode = FilterMode.Bilinear;
            bMat.SetFloat("_BlurScale", blurScale);
            Graphics.Blit(rt, rt2, bMat, vertical ? 5 : 4);
            RenderTexture.ReleaseTemporary(rt);
            return rt2;
        }

        void BlurThisDoF(RenderTexture rt, int renderPass)
        {
            RenderTextureDescriptor desc = rt.descriptor;
            RenderTexture rt2 = RenderTexture.GetTemporary(desc);
            RenderTexture rt3 = RenderTexture.GetTemporary(desc);
            rt2.filterMode = _depthOfFieldFilterMode;
            rt3.filterMode = _depthOfFieldFilterMode;
            UpdateDepthOfFieldBlurData(new Vector2(0.44721f, -0.89443f));
            Graphics.Blit(rt, rt2, bMat, renderPass);
            UpdateDepthOfFieldBlurData(new Vector2(-1f, 0f));
            Graphics.Blit(rt2, rt3, bMat, renderPass);
            UpdateDepthOfFieldBlurData(new Vector2(0.44721f, 0.89443f));
#if !UNITY_STANDALONE && (!UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS || UNITY_WII || UNITY_PS4 || UNITY_XBOXONE || UNITY_TIZEN || UNITY_TVOS || UNITY_WSA || UNITY_WSA_10_0 || UNITY_WEBGL)
            rt.DiscardContents();
#endif
            Graphics.Blit(rt3, rt, bMat, renderPass);
            RenderTexture.ReleaseTemporary(rt3);
            RenderTexture.ReleaseTemporary(rt2);
        }


        void BlurThisAlpha(RenderTexture rt, float blurScale = 1f)
        {
            RenderTextureDescriptor desc = rt.descriptor;
            RenderTexture rt2 = RenderTexture.GetTemporary(desc);
            rt2.filterMode = FilterMode.Bilinear;
            bMat.SetFloat("_BlurScale", blurScale);
            Graphics.Blit(rt, rt2, bMat, 23);
#if !UNITY_STANDALONE && (!UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS || UNITY_WII || UNITY_PS4 || UNITY_XBOXONE || UNITY_TIZEN || UNITY_TVOS || UNITY_WSA || UNITY_WSA_10_0 || UNITY_WEBGL)
            rt.DiscardContents();
#endif
            Graphics.Blit(rt2, rt, bMat, 24);
            RenderTexture.ReleaseTemporary(rt2);
        }

        #endregion

        #region Settings stuff


        void OnDidApplyAnimationProperties()
        {   // support for animating property based fields
            shouldUpdateMaterialProperties = true;
        }

        public void UpdateQualitySettings()
        {
            _depthOfFieldDownsampling = 1;
            _depthOfFieldMaxSamples = 8;
            isDirty = true;
        }

        public void UpdateMaterialProperties()
        {
            if (Application.isPlaying)
            {
                shouldUpdateMaterialProperties = true;
            }
            else
            {
                UpdateMaterialPropertiesNow();
            }
        }

        public void UpdateMaterialPropertiesNow()
        {
            shouldUpdateMaterialProperties = false;

            // Checks camera depth texture mode
            if (currentCamera != null && currentCamera.depthTextureMode == DepthTextureMode.None)
            {
                currentCamera.depthTextureMode = DepthTextureMode.Depth;
            }

            string gpu = SystemInfo.graphicsDeviceName;
            if (gpu != null && gpu.ToUpper().Contains("MALI-T720"))
            {
                rtFormat = RenderTextureFormat.Default;
                _bloomBlur = false; // avoid artifacting due to low precision textures
                _anamorphicFlaresBlur = false;
            }
            else
            {
                rtFormat = SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf) ? RenderTextureFormat.ARGBHalf : RenderTextureFormat.ARGB32;
            }

            if (bMatDesktop == null)
            {
                bMatDesktop = new Material(ResourcesController.test.LoadAsset("Beautify.shader") as Shader);
                bMatDesktop.hideFlags = HideFlags.DontSave;
            }
            bMat = bMatDesktop;

            bool linearColorSpace = (QualitySettings.activeColorSpace == ColorSpace.Linear);

            switch (_preset)
            {
                case BEAUTIFY_PRESET.Soft:
                    _sharpen = 2.0f;
                    if (linearColorSpace)
                        _sharpen *= 2f;
                    _sharpenDepthThreshold = 0.035f;
                    _sharpenRelaxation = 0.065f;
                    _sharpenClamp = 0.4f;
                    _saturate = 0.5f;
                    _contrast = 1.005f;
                    _brightness = 1.05f;
                    _dither = 0.02f;
                    _ditherDepth = 0;
                    _daltonize = 0;
                    break;
                case BEAUTIFY_PRESET.Medium:
                    _sharpen = 3f;
                    if (linearColorSpace)
                        _sharpen *= 2f;
                    _sharpenDepthThreshold = 0.035f;
                    _sharpenRelaxation = 0.07f;
                    _sharpenClamp = 0.45f;
                    _saturate = 1.0f;
                    _contrast = 1.02f;
                    _brightness = 1.05f;
                    _dither = 0.02f;
                    _ditherDepth = 0;
                    _daltonize = 0;
                    break;
                case BEAUTIFY_PRESET.Strong:
                    _sharpen = 4.75f;
                    if (linearColorSpace)
                        _sharpen *= 2f;
                    _sharpenDepthThreshold = 0.035f;
                    _sharpenRelaxation = 0.075f;
                    _sharpenClamp = 0.5f;
                    _saturate = 1.5f;
                    _contrast = 1.03f;
                    _brightness = 1.05f;
                    _dither = 0.022f;
                    _ditherDepth = 0;
                    _daltonize = 0;
                    break;
                case BEAUTIFY_PRESET.Exaggerated:
                    _sharpen = 6f;
                    if (linearColorSpace)
                        _sharpen *= 2f;
                    _sharpenDepthThreshold = 0.035f;
                    _sharpenRelaxation = 0.08f;
                    _sharpenClamp = 0.55f;
                    _saturate = 2.25f;
                    _contrast = 1.035f;
                    _brightness = 1.05f;
                    _dither = 0.025f;
                    _ditherDepth = 0;
                    _daltonize = 0;
                    break;
            }
            isDirty = true;

            if (bMat == null)
                return;
            renderPass = 1;
            if (_pixelateAmount > 1)
            {
                if (QualitySettings.antiAliasing > 1)
                {
                    QualitySettings.antiAliasing = 1;
                }
                if (_pixelateDownscale)
                {
                    _dither = 0;
                }
            }

            // sharpen settings
            UpdateSharpenParams(_sharpen);

            // dither settings
            bool isOrtho = (currentCamera != null && currentCamera.orthographic);
            bMat.SetVector("_Dither", new Vector4(_dither, isOrtho ? 0 : _ditherDepth, (_sharpenMaxDepth + _sharpenMinDepth) * 0.5f, Mathf.Abs(_sharpenMaxDepth - _sharpenMinDepth) * 0.5f + (isOrtho ? 1000.0f : 0f)));
            float cont = linearColorSpace ? 1.0f + (_contrast - 1.0f) / 2.2f : _contrast;

            // color grading settings
            bMat.SetVector("_ColorBoost", new Vector4(_brightness, cont, _saturate, _daltonize * 10f));

            // vignetting FX
            Color vignettingColorAdjusted = _vignettingColor;
            vignettingColorAdjusted.a *= _vignetting ? 32f : 0f;
            float vb = 1f - _vignettingBlink * 2f;
            if (vb < 0) vb = 0;
            vignettingColorAdjusted.r *= vb;
            vignettingColorAdjusted.g *= vb;
            vignettingColorAdjusted.b *= vb;
            bMat.SetColor("_Vignetting", vignettingColorAdjusted);
            if (currentCamera != null)
            {
                bMat.SetFloat("_VignettingAspectRatio", (_vignettingCircularShape && _vignettingBlink <= 0) ? 1.0f / currentCamera.aspect : _vignettingAspectRatio + 1.001f / (1.001f - _vignettingBlink) - 1f);
            }

            // frame FX
            if (_frame)
            {
                Vector4 frameColorAdjusted = new Vector4(_frameColor.r, _frameColor.g, _frameColor.b, (1.00001f - _frameColor.a) * 0.5f);
                bMat.SetVector("_Frame", frameColorAdjusted);
            }

            // outline FX
            bMat.SetColor("_Outline", _outlineColor);

            // bloom
            float bloomWeightsSum = 0.00001f + _bloomWeight0 + _bloomWeight1 + _bloomWeight2 + _bloomWeight3 + _bloomWeight4 + _bloomWeight5;
            bMat.SetVector("_BloomWeights", new Vector4(_bloomWeight0 / bloomWeightsSum + _bloomBoost0, _bloomWeight1 / bloomWeightsSum + _bloomBoost1, _bloomWeight2 / bloomWeightsSum + _bloomBoost2, _bloomWeight3 / bloomWeightsSum + _bloomBoost3));
            bMat.SetVector("_BloomWeights2", new Vector4(_bloomWeight4 / bloomWeightsSum + _bloomBoost4, _bloomWeight5 / bloomWeightsSum + _bloomBoost5, _bloomMaxBrightness, bloomWeightsSum));
            if (_bloomDebug && (_bloom || _anamorphicFlares))
                renderPass = 3;

            // lens dirt
            //  if (_lensDirtTexture == null)
            //   {
            //    _lensDirtTexture = Resources.Load<Texture2D>("Textures/dirt2") as Texture2D;
            //  }
            bMat.SetTexture("_OverlayTex", _lensDirtTexture);

            // anamorphic flares
            bMat.SetColor("_AFTint", _anamorphicFlaresTint);

            // dof
            if (_depthOfField && _depthOfFieldAutofocusLayerMask != 0)
            {
                Shader.SetGlobalFloat("_BeautifyDepthBias", _depthOfFieldExclusionBias);
            }
            dofCurrentLayerMaskValue = _depthOfFieldExclusionLayerMask.value;

            // final config
            if (shaderKeywords == null)
                shaderKeywords = new List<string>();
            else
                shaderKeywords.Clear();


            if (_lut && _lutTexture != null)
            {
                shaderKeywords.Add(SKW_LUT);
                bMat.SetTexture("_LUTTex", _lutTexture);
                bMat.SetColor("_FXColor", new Color(0, 0, 0, _lutIntensity));
            }
            else if (_nightVision)
            {
                shaderKeywords.Add(SKW_NIGHT_VISION);
                Color nightVisionAdjusted = _nightVisionColor;
                if (linearColorSpace)
                {
                    nightVisionAdjusted.a *= 5.0f * nightVisionAdjusted.a;
                }
                else
                {
                    nightVisionAdjusted.a *= 3.0f * nightVisionAdjusted.a;
                }
                nightVisionAdjusted.r = nightVisionAdjusted.r * nightVisionAdjusted.a;
                nightVisionAdjusted.g = nightVisionAdjusted.g * nightVisionAdjusted.a;
                nightVisionAdjusted.b = nightVisionAdjusted.b * nightVisionAdjusted.a;
                bMat.SetColor("_FXColor", nightVisionAdjusted);
            }
            else if (_thermalVision)
            {
                shaderKeywords.Add(SKW_THERMAL_VISION);
            }
            else if (_daltonize > 0)
            {
                shaderKeywords.Add(SKW_DALTONIZE);
            }
            else
            { // set _FXColor for procedural sepia
                bMat.SetColor("_FXColor", new Color(0, 0, 0, _lutIntensity));
            }
            bMat.SetColor("_TintColor", _tintColor);


            if (_vignetting)
            {
                if (_vignettingMask != null)
                {
                    bMat.SetTexture("_VignettingMask", _vignettingMask);
                    shaderKeywords.Add(SKW_VIGNETTING_MASK);
                }
                else
                {
                    shaderKeywords.Add(SKW_VIGNETTING);
                }
            }
            if (_frame)
            {
                if (_frameMask != null)
                {
                    bMat.SetTexture("_FrameMask", _frameMask);
                    shaderKeywords.Add(SKW_FRAME_MASK);
                }
                else
                {
                    shaderKeywords.Add(SKW_FRAME);
                }
            }
            if (_outline)
                shaderKeywords.Add(SKW_OUTLINE);
            if (_lensDirt)
                shaderKeywords.Add(SKW_DIRT);
            if (_bloom || _anamorphicFlares)
            {
                shaderKeywords.Add(SKW_BLOOM);
                if (_bloomDepthAtten > 0)
                {
                    bMat.SetFloat("_BloomDepthTreshold", _bloomDepthAtten);
                    shaderKeywords.Add(SKW_BLOOM_USE_DEPTH);
                }
                if ((_bloom || _anamorphicFlares) && _bloomCullingMask != 0)
                {
                    bMat.SetFloat("_BloomLayerZBias", _bloomLayerZBias);
                    shaderKeywords.Add(SKW_BLOOM_USE_LAYER);
                }
            }
            if (_depthOfField)
            {
                if (_depthOfFieldTransparencySupport || _depthOfFieldExclusionLayerMask != 0)
                {
                    shaderKeywords.Add(SKW_DEPTH_OF_FIELD_TRANSPARENT);
                }
                else
                {
                    shaderKeywords.Add(SKW_DEPTH_OF_FIELD);
                }
            }
            if (_eyeAdaptation)
            {
                Vector4 eaData = new Vector4(_eyeAdaptationMinExposure, _eyeAdaptationMaxExposure, _eyeAdaptationSpeedToDark, _eyeAdaptationSpeedToLight);
                bMat.SetVector("_EyeAdaptation", eaData);
                shaderKeywords.Add(SKW_EYE_ADAPTATION);
            }
            if (_quality == BEAUTIFY_QUALITY.BestQuality)
            {
                if (_tonemap == BEAUTIFY_TMO.ACES)
                    shaderKeywords.Add(SKW_TONEMAP_ACES);
            }
            if (_purkinje || _vignetting)
            {
                float vd = _vignettingFade + _vignettingBlink * 0.5f;
                if (_vignettingBlink > 0.99f) vd = 1f;
                Vector3 purkinjeData = new Vector3(_purkinjeAmount, _purkinjeLuminanceThreshold, vd);
                bMat.SetVector("_Purkinje", purkinjeData);
                shaderKeywords.Add(SKW_PURKINJE);
            }
            bMat.shaderKeywords = shaderKeywords.ToArray();

#if DEBUG_BEAUTIFY
												Debug.Log("*** DEBUG: Updating material properties...");
												Debug.Log("Linear color space: " + linearColorSpace.ToString());
												Debug.Log("Preset: " + _preset.ToString());
												Debug.Log("Sharpen: " + _sharpen.ToString());
												Debug.Log("Dither: " + _dither.ToString());
												Debug.Log("Contrast: " + cont.ToString());
												Debug.Log("Bloom: " + _bloom.ToString());
												Debug.Log("Bloom Intensity: " + _bloomIntensity.ToString());
												Debug.Log("Bloom Threshold: " + bloomThreshold.ToString());
												Debug.Log("Bloom Weight: " + bloomWeightsSum.ToString());
#endif
        }

        void UpdateMaterialBloomIntensityAndThreshold()
        {
            float threshold = _bloomThreshold;
            if (QualitySettings.activeColorSpace == ColorSpace.Linear)
            {
                threshold *= threshold;
            }
            bMat.SetVector("_Bloom", new Vector4(_bloomIntensity + (_anamorphicFlares ? 0.0001f : 0f), 0, 0, threshold));
        }

        void UpdateMaterialAnamorphicIntensityAndThreshold()
        {
            float threshold = _anamorphicFlaresThreshold;
            if (QualitySettings.activeColorSpace == ColorSpace.Linear)
            {
                threshold *= threshold;
            }
            float intensity = _anamorphicFlaresIntensity / (_bloomIntensity + 0.0001f);
            bMat.SetVector("_Bloom", new Vector4(intensity, 0, 0, threshold));
        }

        void UpdateSharpenParams(float sharpen)
        {
            bMat.SetVector("_Sharpen", new Vector4(sharpen, _sharpenDepthThreshold, _sharpenClamp, _sharpenRelaxation));
        }

        void UpdateDepthOfFieldData()
        {
            // TODO: get focal length from camera FOV: FOV = 2 arctan (x/2f) x = diagonal of film (0.024mm)
            float d;
            if (_depthOfFieldAutofocus)
            {
                UpdateDoFAutofocusDistance();
                d = dofLastAutofocusDistance > 0 ? dofLastAutofocusDistance : currentCamera.farClipPlane;
            }
            else if (_depthOfFieldTargetFocus != null)
            {
                Vector3 spos = currentCamera.WorldToScreenPoint(_depthOfFieldTargetFocus.position);
                if (spos.z < 0)
                {
                    d = currentCamera.farClipPlane;
                }
                else
                {
                    d = Vector3.Distance(currentCamera.transform.position, _depthOfFieldTargetFocus.position);
                }
            }
            else
            {
                d = _depthOfFieldDistance;
            }
            dofPrevDistance = Mathf.Lerp(dofPrevDistance, d, Application.isPlaying ? _depthOfFieldFocusSpeed * Time.deltaTime * 30f : 1f);
            float dofCoc = _depthOfFieldAperture * (_depthOfFieldFocalLength / Mathf.Max(dofPrevDistance - _depthOfFieldFocalLength, 0.001f)) * (1f / 0.024f);
            dofLastBokehData = new Vector4(dofPrevDistance, dofCoc, 0, 0);
            bMat.SetVector("_BokehData", dofLastBokehData);
            bMat.SetVector("_BokehData2", new Vector4(_depthOfFieldForegroundBlur ? _depthOfFieldForegroundDistance : currentCamera.farClipPlane, _depthOfFieldMaxSamples, _depthOfFieldBokehThreshold, _depthOfFieldBokehIntensity * _depthOfFieldBokehIntensity));
            bMat.SetFloat("_BokehData3", _depthOfFieldMaxBrightness);
        }

        void UpdateDepthOfFieldBlurData(Vector2 blurDir)
        {
            float downsamplingRatio = 1f / (float)_depthOfFieldDownsampling;
            blurDir *= downsamplingRatio;
            dofLastBokehData.z = blurDir.x;
            dofLastBokehData.w = blurDir.y;
            bMat.SetVector("_BokehData", dofLastBokehData);
        }

        void UpdateDoFAutofocusDistance()
        {
            Ray r = new Ray(currentCamera.transform.position, currentCamera.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, currentCamera.farClipPlane, _depthOfFieldAutofocusLayerMask))
            {
                dofLastAutofocusDistance = Mathf.Clamp(hit.distance, _depthOfFieldAutofocusMinDistance, _depthOfFieldAutofocusMaxDistance);
            }
            else
            {
                dofLastAutofocusDistance = currentCamera.farClipPlane;
            }
        }

        public Texture2D GenerateSepiaLUT()
        {
            Texture2D tex = new Texture2D(1024, 32, TextureFormat.ARGB32, false, true);
            Color[] colors = new Color[1024 * 32];
            for (int y = 0; y < 32; y++)
            {
                for (int x = 0; x < 1024; x++)
                {
                    Vector3 rgb;
                    rgb.z = ((x / 32) * 8) / 255f;
                    rgb.y = (y * 8) / 255f;
                    rgb.x = ((x % 32) * 8) / 255f;
                    float sepiaR = Vector3.Dot(rgb, new Vector3(0.393f, 0.769f, 0.189f));
                    float sepiaG = Vector3.Dot(rgb, new Vector3(0.349f, 0.686f, 0.168f));
                    float sepiaB = Vector3.Dot(rgb, new Vector3(0.272f, 0.534f, 0.131f));
                    colors[y * 1024 + x] = new Color(sepiaR, sepiaG, sepiaB);
                }
            }
            tex.SetPixels(colors);
            tex.Apply();
            return tex;
        }

        #endregion

        #region API

        /// <summary>
        /// Animates blink parameter
        /// </summary>
        /// <returns>The blink.</returns>
        /// <param name="duration">Duration.</param>
        public void Blink(float duration, float maxValue = 1)
        {
            if (duration <= 0) return;
            StartCoroutine(DoBlink(duration, maxValue));
        }

        IEnumerator DoBlink(float duration, float maxValue)
        {

            float start = Time.time;
            float t = 0;
            WaitForEndOfFrame w = new WaitForEndOfFrame();

            // Close
            do
            {
                t = (Time.time - start) / duration;
                if (t > 1f) t = 1f;
                float easeOut = t * (2f - t);
                vignettingBlink = easeOut * maxValue;
                yield return w;
            } while (t < 1f);

            // Open
            start = Time.time;
            do
            {
                t = (Time.time - start) / duration;
                if (t > 1f) t = 1f;
                float easeIn = t * t;
                vignettingBlink = (1f - easeIn) * maxValue;
                yield return w;
            } while (t < 1f);
        }

        #endregion

        #region GUI_WIP
        private bool openGUI = false;

        private void OnGUI()
        {
            /* if (!openGUI) return;
             // setup styles
             if (labelBoldStyle == null)
             {
                 labelBoldStyle = new GUIStyle(GUI.skin.label); // GUI.skin.label);
                 labelBoldStyle.fontStyle = FontStyle.Bold;
             }
             if (labelNormalStyle == null)
             {
                 labelNormalStyle = new GUIStyle(GUI.skin.label); // GUI.skin.label);
             }
             if (sectionHeaderNormalStyle == null)
             {
                 sectionHeaderNormalStyle = new GUIStyle(EditorStyles.foldout);
             }
             sectionHeaderNormalStyle.margin = new RectOffset(12, 0, 0, 0);
             if (sectionHeaderBoldStyle == null)
             {
                 sectionHeaderBoldStyle = new GUIStyle(sectionHeaderNormalStyle);
             }
             sectionHeaderBoldStyle.fontStyle = FontStyle.Bold;
             if (sectionHeaderIndentedStyle == null)
             {
                 sectionHeaderIndentedStyle = new GUIStyle(EditorStyles.foldout);
             }
             sectionHeaderIndentedStyle.margin = new RectOffset(24, 0, 0, 0);
             if (buttonNormalStyle == null)
             {
                 buttonNormalStyle = new GUIStyle(GUI.skin.button); // EditorStyles.miniButtonMid);
             }
             if (buttonPressedStyle == null)
             {
                 buttonPressedStyle = new GUIStyle(buttonNormalStyle);
                 buttonPressedStyle.fontStyle = FontStyle.Bold;
             }
             if (foldoutBold == null)
             {
                 foldoutBold = new GUIStyle(EditorStyles.foldout);
                 foldoutBold.fontStyle = FontStyle.Bold;
             }
             if (foldoutNormal == null)
             {
                 foldoutNormal = new GUIStyle(EditorStyles.foldout);
             }
             if (foldOutIndented == null)
             {
                 foldOutIndented = new GUIStyle(EditorStyles.foldout);
             }
             foldOutIndented.margin = new RectOffset(26, 0, 0, 0);

             bool qualityChanged = false;
             //  bool profileChanged = false;

             // draw interface
             EditorGUIUtility.labelWidth = 125;
             GUILayout.Separator();
             GUI.skin.label.alignment = TextAnchor.MiddleCenter;
             if (!_effect.enabled)
             {
                 GUILayout.HelpBox("Beautify disabled.", MessageType.Info);
             }
             GUILayout.Separator();

             GUILayout.BeginHorizontal();
             DrawLabel("General Settings");
             GUILayout.EndHorizontal();

             GUILayout.EndHorizontal();

             if (EditorGUI.EndChangeCheck())
             {
                 generalSettingsChanged = true;
             }

             BeautifySInfo shaderInfo = null;
             if (shaders.Count > 0)
             {
                 shaderInfo = shaders[0];
             }

             if (shaderInfo != null && shaderInfo.enabledKeywordCount >= 17)
             {
                 GUILayout.HelpBox("Please remember to disable unwanted effects in Build Options before building your game!", MessageType.Warning);
             }

             GUILayout.Separator();
             DrawLabel("Image Enhancement");

 #if UNITY_5_6_OR_NEWER
             if (_effect.cameraEffect != null && !_effect.cameraEffect.allowHDR)
             {
 #else
                                                 if (_effect.cameraEffect != null && !_effect.cameraEffect.hdr) {
 #endif
                 GUILayout.HelpBox("Some effects, like dither and bloom, works better with HDR enabled. Check your camera setting.", MessageType.Warning);
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(121));
             expandSharpenSection = GUILayout.Foldout(expandSharpenSection, new GUIContent("Sharpen", "Sharpen intensity."), sectionHeaderNormalStyle);
             GUILayout.EndHorizontal();
             GUILayout.PropertyField(_sharpen, GUIContent.none);
             GUILayout.EndHorizontal();

             if (expandSharpenSection)
             {
                 if (_effect.cameraEffect != null && !_effect.cameraEffect.orthographic)
                 {

                     GUILayout.BeginHorizontal();
                     GUILayout.Label(new GUIContent("   Min/Max Depth", "Any pixel outside this depth range won't be affected by sharpen. Reduce range to create a depth-of-field-like effect."), GUILayout.Width(121));
                     float minDepth = _sharpenMinDepth.floatValue;
                     float maxDepth = _sharpenMaxDepth.floatValue;
                     GUILayout.MinMaxSlider(ref minDepth, ref maxDepth, 0f, 1.1f);
                     _sharpenMinDepth.floatValue = minDepth;
                     _sharpenMaxDepth.floatValue = maxDepth;
                     GUILayout.EndHorizontal();
                     GUILayout.PropertyField(_sharpenDepthThreshold, new GUIContent("   Depth Threshold", "Reduces sharpen if depth difference around a pixel exceeds this value. Useful to prevent artifacts around wires or thin objects."));
                 }
                 GUILayout.PropertyField(_sharpenRelaxation, new GUIContent("   Luminance Relax.", "Soften sharpen around a pixel with high contrast. Reduce this value to remove ghosting and protect fine drawings or wires over a flat surface."));
                 GUILayout.PropertyField(_sharpenClamp, new GUIContent("   Clamp", "Maximum pixel adjustment."));
                 GUILayout.PropertyField(_sharpenMotionSensibility, new GUIContent("   Motion Sensibility", "Increase to reduce sharpen to simulate a cheap motion blur and to reduce flickering when camera rotates or moves. This slider controls the amount of camera movement/rotation that contributes to sharpen reduction. Set this to 0 to disable this feature."));
             }



             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(121));
             expandDitherSection = GUILayout.Foldout(expandDitherSection, new GUIContent("Dither", "Simulates more colors than RGB quantization can produce. Removes banding artifacts in gradients, like skybox. This setting controls the dithering strength."), sectionHeaderNormalStyle);
             GUILayout.EndHorizontal();
             GUILayout.PropertyField(_dither, GUIContent.none);
             GUILayout.EndHorizontal();
             if (expandDitherSection)
             {
                 if (_effect.cameraEffect != null && !_effect.cameraEffect.orthographic)
                 {
                     GUILayout.PropertyField(_ditherDepth, new GUIContent("   Min Depth", "Will only remove bands on pixels beyond this depth. Useful if you only want to remove sky banding (set this to 0.99)"));
                 }
             }


             bool isLUTEnabled = isShaderFeatureEnabled(Beautify.SKW_LUT, false);
             if (!isLUTEnabled)
             {
                 if (_lut.boolValue)
                 {
                     _lut.boolValue = false;
                 }
             }

             GUILayout.Separator();
             DrawLabel("Tonemapping & Color Grading");

             if (_quality.intValue != (int)BEAUTIFY_QUALITY.BestQuality)
             {
                 GUI.enabled = false;
             }

             int prevTonemap = _tonemap.intValue;
             GUILayout.BeginHorizontal();
             GUIStyle labelStyle = _tonemap.intValue != (int)BEAUTIFY_TMO.Linear ? labelBoldStyle : labelNormalStyle;
             GUILayout.Label(new GUIContent("Tonemapping", "Converts high dynamic range colors into low dynamic range space according to a chosen tone mapping operator."), labelStyle, GUILayout.Width(121));
             if (isShaderFeatureEnabled(Beautify.SKW_TONEMAP_ACES))
             {
                 GUILayout.PropertyField(_tonemap, GUIContent.none);
             }
             else
             {
                 _tonemap.intValue = (int)BEAUTIFY_TMO.Linear;
             }
             GUILayout.EndHorizontal();
             GUI.enabled = true;
             if (prevTonemap != _tonemap.intValue && _tonemap.intValue == (int)BEAUTIFY_TMO.ACES)
             {
                 _saturate.floatValue = 0;
                 _contrast.floatValue = 1f;
             }

             if (_tonemap.intValue != (int)BEAUTIFY_TMO.Linear)
             {
                 GUILayout.PropertyField(_brightness, new GUIContent("Exposure", "Exposure applied before tonemapping. Increase to make the image brighter."));
             }

             GUILayout.PropertyField(_saturate, new GUIContent("Vibrance", "Improves pixels color depending on their saturation."));


             GUILayout.BeginHorizontal();
             labelStyle = _daltonize.floatValue > 0 ? labelBoldStyle : labelNormalStyle;
             GUILayout.Label(new GUIContent("Daltonize", "Similar to vibrance but mostly accentuate primary red, green and blue colors to compensate protanomaly (red deficiency), deuteranomaly (green deficiency) and tritanomaly (blue deficiency). This effect does not shift color hue hence it won't help completely red, green or blue color blindness. The effect will vary depending on each subject so this effect should be enabled on user demand."), labelStyle, GUILayout.Width(121));
             if (isShaderFeatureEnabled(Beautify.SKW_DALTONIZE))
             {
                 GUILayout.PropertyField(_daltonize, GUIContent.none);
             }
             else
             {
                 _daltonize.floatValue = 0f;
             }
             GUILayout.EndHorizontal();
             if (_daltonize.floatValue > 0 && _lut.boolValue)
                 GUILayout.HelpBox("Daltonize disabled by LUT.", MessageType.Info);

             GUILayout.BeginHorizontal();
             labelStyle = _tintColor.colorValue.a > 0 ? labelBoldStyle : labelNormalStyle;
             GUILayout.Label(new GUIContent("Tint", "Blends image with an optional color. Alpha specifies intensity."), labelStyle, GUILayout.Width(121));
             GUILayout.PropertyField(_tintColor, GUIContent.none);
             GUILayout.EndHorizontal();

             // bool sepiaMode = shaderAdvancedOptionsInfo != null && shaderAdvancedOptionsInfo.GetAdvancedOptionState("BEAUTIFY_USE_PROCEDURAL_SEPIA");
             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _lut.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandLUTSection = GUILayout.Foldout(expandLUTSection, new GUIContent("LUT", "Enables LUT based transformation effects."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isLUTEnabled))
             {
                 GUILayout.PropertyField(_lut, GUIContent.none);
                 GUILayout.EndHorizontal();
                 if (expandLUTSection)
                 {
                     GUILayout.BeginHorizontal();
                     GUILayout.PropertyField(_lutTexture, new GUIContent("   Texture"));
                     GUILayout.EndHorizontal();
                     GUILayout.PropertyField(_lutIntensity, new GUIContent("   Intensity"));
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.PropertyField(_contrast, new GUIContent("Contrast", "Final image contrast adjustment. Allows you to create more vivid images."));

             if (_tonemap.intValue == (int)BEAUTIFY_TMO.Linear)
             {
                 GUILayout.PropertyField(_brightness, new GUIContent("Brightness", "Final image brightness adjustment."));
             }



             bool isBloomEnabled = isShaderFeatureEnabled(Beautify.SKW_BLOOM, false);
             if (!isBloomEnabled)
             {
                 if (_bloom.boolValue)
                 {
                     _bloom.boolValue = false;
                 }
                 if (_anamorphicFlares.boolValue)
                 {
                     _anamorphicFlares.boolValue = false;
                 }
             }
             bool isLensDirtEnabled = isShaderFeatureEnabled(Beautify.SKW_DIRT, false);
             if (!isLensDirtEnabled)
             {
                 if (_lensDirt.boolValue)
                 {
                     _lensDirt.boolValue = false;
                 }
             }


             GUILayout.Separator();
             DrawLabel("Lens & Lighting Effects");

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(121));
             labelStyle = _bloom.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandBloomSection = GUILayout.Foldout(expandBloomSection, new GUIContent("Bloom", "Produces fringes of light extending from the borders of bright areas, contributing to the illusion of an extremely bright light overwhelming the camera or eye capturing the scene."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isBloomEnabled))
             {
                 GUILayout.PropertyField(_bloom, GUIContent.none);
                 if (expandBloomSection)
                 {
                     if (_bloom.boolValue)
                     {
                         GUILayout.Label(new GUIContent("Debug", "Enable to see flares buffer."));
                         _bloomDebug.boolValue = GUILayout.Toggle(_bloomDebug.boolValue, GUILayout.Width(40));
                     }
                     GUILayout.EndHorizontal();
                     GUILayout.BeginHorizontal();
                     labelStyle = _bloomCullingMask.intValue != 0 ? labelBoldStyle : labelNormalStyle;
                     GUILayout.Label(new GUIContent("   Layer Mask", "Select which layers can be used for bloom."), labelStyle, GUILayout.Width(121));
                     GUILayout.PropertyField(_bloomCullingMask, GUIContent.none);
                     GUILayout.EndHorizontal();
                     if (_bloom.boolValue)
                     {
                         if ((_bloomCullingMask.intValue & 1) != 0)
                         {
                             GUILayout.HelpBox("Set Layer Mask either to Nothing (default value) or to specific layers. Including Default layer is not recommended. If you want bloom to be applied to all objects, set Layer Mask to Nothing.", MessageType.Warning);
                         }
                     }
                     if (_bloomCullingMask.intValue != 0)
                     {
                         if (_quality.intValue == (int)BEAUTIFY_QUALITY.BestQuality)
                         {
                             GUILayout.PropertyField(_bloomLayerMaskDownsampling, new GUIContent("   Mask Downsampling", "Bloom/anamorphic flares layer mask downsampling factor. Increase to improve performance."));
                         }
                         GUILayout.PropertyField(_bloomLayerZBias, new GUIContent("   Mask Z Bias", "Optionally applies a small offset (ie. 0.01) to the depth comparisson between objects in the bloom layer and others, enabling bloom effect behind opaque objects (similar to translucency effect)."));
                     }
                     GUILayout.PropertyField(_bloomIntensity, new GUIContent("   Intensity", "Bloom multiplier."));
                     GUILayout.PropertyField(_bloomThreshold, new GUIContent("   Threshold", "Brightness sensibility."));

                     GUILayout.BeginHorizontal();
                     labelStyle = _bloomDepthAtten.floatValue > 0 ? labelBoldStyle : labelNormalStyle;
                     GUILayout.Label(new GUIContent("   Depth Atten", "Reduces bloom effect on distance."), labelStyle, GUILayout.Width(121));
                     GUILayout.PropertyField(_bloomDepthAtten, GUIContent.none);
                     GUILayout.EndHorizontal();

                     GUILayout.PropertyField(_bloomMaxBrightness, new GUIContent("   Clamp Brightness", "Clamps maximum pixel brightness to prevent out of range bright spots."));
                     if (_quality.intValue == (int)BEAUTIFY_QUALITY.BestQuality)
                     {
                         GUILayout.PropertyField(_bloomAntiflicker, new GUIContent("   Reduce Flicker", "Enables an additional filter to reduce excess of flicker."));
                         GUILayout.PropertyField(_bloomUltra, new GUIContent("   Ultra", "Increase bloom fidelity."));

                         bool prevCustomize = _bloomCustomize.boolValue;
                         GUILayout.PropertyField(_bloomCustomize, new GUIContent("   Customize", "Edit bloom style parameters."));
                         if (_bloomCustomize.boolValue)
                         {
                             if (!prevCustomize)
                             {
                                 layer1 = layer2 = layer3 = layer4 = layer5 = layer6 = true;
                             }
                             GUILayout.BeginHorizontal();
                             GUILayout.Label("   Presets", GUILayout.Width(120));
                             if (GUILayout.Button("Focused"))
                             {
                                 _bloomWeight0.floatValue = 1f;
                                 _bloomWeight1.floatValue = 0.9f;
                                 _bloomWeight2.floatValue = 0.75f;
                                 _bloomWeight3.floatValue = 0.6f;
                                 _bloomWeight4.floatValue = 0.35f;
                                 _bloomWeight5.floatValue = 0.1f;
                                 _bloomBoost0.floatValue = _bloomBoost1.floatValue = _bloomBoost2.floatValue = _bloomBoost3.floatValue = _bloomBoost4.floatValue = _bloomBoost5.floatValue = 0;
                             }
                             if (GUILayout.Button("Regular"))
                             {
                                 _bloomWeight0.floatValue = 0.25f;
                                 _bloomWeight1.floatValue = 0.33f;
                                 _bloomWeight2.floatValue = 0.8f;
                                 _bloomWeight3.floatValue = 1f;
                                 _bloomWeight4.floatValue = 1f;
                                 _bloomWeight5.floatValue = 1f;
                                 _bloomBoost0.floatValue = _bloomBoost1.floatValue = _bloomBoost2.floatValue = _bloomBoost3.floatValue = _bloomBoost4.floatValue = _bloomBoost5.floatValue = 0;
                             }
                             if (GUILayout.Button("Blurred"))
                             {
                                 _bloomWeight0.floatValue = 0.05f;
                                 _bloomWeight1.floatValue = 0.075f;
                                 _bloomWeight2.floatValue = 0.1f;
                                 _bloomWeight3.floatValue = 0.2f;
                                 _bloomWeight4.floatValue = 0.4f;
                                 _bloomWeight5.floatValue = 1f;
                                 _bloomBoost0.floatValue = _bloomBoost1.floatValue = _bloomBoost2.floatValue = _bloomBoost3.floatValue = 0;
                                 _bloomBoost4.floatValue = 0.5f;
                                 _bloomBoost5.floatValue = 1f;
                             }
                             GUILayout.EndHorizontal();

                             layer1 = GUILayout.Foldout(layer1, "Layer 1", foldOutIndented);
                             if (layer1)
                             {
                                 GUILayout.PropertyField(_bloomWeight0, new GUIContent("      Weight", "First layer bloom weight."));
                                 GUILayout.PropertyField(_bloomBoost0, new GUIContent("      Boost", "Intensity bonus for first layer."));
                             }
                             layer2 = GUILayout.Foldout(layer2, "Layer 2", foldOutIndented);
                             if (layer2)
                             {
                                 GUILayout.PropertyField(_bloomWeight1, new GUIContent("      Weight", "Second layer bloom weight."));
                                 GUILayout.PropertyField(_bloomBoost1, new GUIContent("      Boost", "Intensity bonus for second layer."));
                             }
                             layer3 = GUILayout.Foldout(layer3, "Layer 3", foldOutIndented);
                             if (layer3)
                             {
                                 GUILayout.PropertyField(_bloomWeight2, new GUIContent("      Weight", "Third layer bloom weight."));
                                 GUILayout.PropertyField(_bloomBoost2, new GUIContent("      Boost", "Intensity bonus for third layer."));
                             }
                             layer4 = GUILayout.Foldout(layer4, "Layer 4", foldOutIndented);
                             if (layer4)
                             {
                                 GUILayout.PropertyField(_bloomWeight3, new GUIContent("      Weight", "Fourth layer bloom weight."));
                                 GUILayout.PropertyField(_bloomBoost3, new GUIContent("      Boost", "Intensity bonus for fourth layer."));
                             }
                             layer5 = GUILayout.Foldout(layer5, "Layer 5", foldOutIndented);
                             if (layer5)
                             {
                                 GUILayout.PropertyField(_bloomWeight4, new GUIContent("      Weight", "Fifth layer bloom weight."));
                                 GUILayout.PropertyField(_bloomBoost4, new GUIContent("      Boost", "Intensity bonus for fifth layer."));
                             }
                             layer6 = GUILayout.Foldout(layer6, "Layer 6", foldOutIndented);
                             if (layer6)
                             {
                                 GUILayout.PropertyField(_bloomWeight5, new GUIContent("      Weight", "Sixth layer bloom weight."));
                                 GUILayout.PropertyField(_bloomBoost5, new GUIContent("      Boost", "Intensity bonus for sixth layer."));
                             }
                         }
                     }
                     else
                     {
                         GUILayout.PropertyField(_bloomBlur, new GUIContent("   Blur", "Adds an additional blur pass to smooth bloom."));
                     }
                 }
                 else
                 {
                     GUILayout.EndHorizontal();
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _anamorphicFlares.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandAFSection = GUILayout.Foldout(expandAFSection, new GUIContent("Anamorphic F.", "Also known as JJ Abrams flares, adds spectacular light streaks to your scene."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isBloomEnabled))
             {
                 GUILayout.PropertyField(_anamorphicFlares, GUIContent.none);
                 if (expandAFSection)
                 {
                     if (_anamorphicFlares.boolValue)
                     {
                         GUILayout.Label(new GUIContent("Debug", "Enable to see flares buffer."));
                         _bloomDebug.boolValue = GUILayout.Toggle(_bloomDebug.boolValue, GUILayout.Width(40));
                     }
                     GUILayout.EndHorizontal();
                     GUILayout.PropertyField(_bloomCullingMask, new GUIContent("   Layer Mask", "Select which layers can be used for bloom."));
                     if (_anamorphicFlares.boolValue)
                     {
                         if ((_bloomCullingMask.intValue & 1) != 0)
                         {
                             GUILayout.HelpBox("Set Layer Mask either to Nothing (default value) or to specific layers. Including Default layer is not recommended. If you want flares to be applied to all objects, set Layer Mask to Nothing.", MessageType.Warning);
                         }
                     }
                     if (_bloomCullingMask.intValue != 0 && _quality.intValue == (int)BEAUTIFY_QUALITY.BestQuality)
                     {
                         GUILayout.PropertyField(_bloomLayerMaskDownsampling, new GUIContent("   Mask Downsampling", "Bloom/anamorphic flares layer mask downsampling factor. Increase to improve performance."));
                     }
                     GUILayout.PropertyField(_anamorphicFlaresIntensity, new GUIContent("   Intensity", "Flares light multiplier."));
                     GUILayout.PropertyField(_anamorphicFlaresThreshold, new GUIContent("   Threshold", "Brightness sensibility."));
                     GUILayout.PropertyField(_anamorphicFlaresSpread, new GUIContent("   Spread", "Amplitude of the flares."));
                     GUILayout.PropertyField(_anamorphicFlaresVertical, new GUIContent("   Vertical"));
                     GUILayout.PropertyField(_anamorphicFlaresTint, new GUIContent("   Tint", "Optional tint color for the anamorphic flares. Use color alpha component to blend between original color and the tint."));

                     if (_quality.intValue == (int)BEAUTIFY_QUALITY.BestQuality)
                     {
                         GUILayout.PropertyField(_anamorphicFlaresAntiflicker, new GUIContent("   Reduce Flicker", "Enables an additional filter to reduce excess of flicker."));
                         GUILayout.PropertyField(_anamorphicFlaresUltra, new GUIContent("   Ultra", "Increases anamorphic flares fidelity."));
                     }
                     else
                     {
                         GUILayout.PropertyField(_anamorphicFlaresBlur, new GUIContent("   Blur", "Adds an additional blur pass to smooth anamorphic flare effect."));
                     }
                 }
                 else
                 {
                     GUILayout.EndHorizontal();
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _lensDirt.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandDirtSection = GUILayout.Foldout(expandDirtSection, new GUIContent("Lens Dirt", "Enables lens dirt effect which intensifies when looking to a light (uses the nearest light to camera). You can assign other dirt textures directly to the shader material with name 'Beautify'."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isLensDirtEnabled))
             {
                 _lensDirt.boolValue = GUILayout.Toggle(_lensDirt.boolValue);
                 if (expandDirtSection)
                 {
                     GUILayout.EndHorizontal();
                     GUILayout.BeginHorizontal();
                     GUILayout.PropertyField(_lensDirtTexture, new GUIContent("   Dirt Texture", "Texture used for the lens dirt effect."));
                     GUILayout.EndHorizontal();
                     GUILayout.PropertyField(_lensDirtThreshold, new GUIContent("   Threshold", "This slider controls the visibility of lens dirt. A high value will make lens dirt only visible when looking directly towards a light source. A lower value will make lens dirt visible all time."));
                     GUILayout.PropertyField(_lensDirtIntensity, new GUIContent("   Intensity", "This slider controls the maximum brightness of lens dirt effect."));

                 }
                 else
                 {
                     GUILayout.EndHorizontal();
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             bool isDoFEnabled = isShaderFeatureEnabled(Beautify.SKW_DEPTH_OF_FIELD, false);
             if (!isDoFEnabled)
             {
                 if (_depthOfField.boolValue)
                 {
                     _depthOfField.boolValue = false;
                 }
             }
             bool isDoFTransp = isShaderFeatureEnabled(Beautify.SKW_DEPTH_OF_FIELD_TRANSPARENT, false);
             if (!isDoFTransp)
             {
                 _depthOfFieldTransparencySupport.boolValue = false;
                 _depthOfFieldExclusionLayerMask.intValue = 0;
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _depthOfField.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandDoFSection = GUILayout.Foldout(expandDoFSection, new GUIContent("Depth of Field", "Blurs the image based on distance to focus point."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isDoFEnabled))
             {
                 _depthOfField.boolValue = GUILayout.Toggle(_depthOfField.boolValue);
                 if (expandDoFSection)
                 {
                     if (_depthOfField.boolValue)
                     {
                         GUILayout.Label(new GUIContent("Debug", "Enable to see depth of field focus area."));
                         _depthOfFieldDebug.boolValue = GUILayout.Toggle(_depthOfFieldDebug.boolValue, GUILayout.Width(40));
                     }
                     GUILayout.EndHorizontal();
                     GUILayout.PropertyField(_depthOfFieldAutofocus, new GUIContent("   Autofocus", "Automatically focus the object in front of camera."));

                     if (_depthOfFieldAutofocus.boolValue)
                     {
                         GUILayout.PropertyField(_depthOfFieldAutofocusLayerMask, new GUIContent("      Layer Mask", "Select which layers can be used for autofocus option."));
                         GUILayout.PropertyField(_depthOfFieldAutofocusMinDistance, new GUIContent("      Min Distance", "Minimum distance accepted for any focused object."));
                         GUILayout.PropertyField(_depthOfFieldAutofocusMaxDistance, new GUIContent("      Max Distance", "Maximum distance accepted for any focused object."));
                     }
                     else
                     {
                         GUILayout.PropertyField(_depthOfFieldDistance, new GUIContent("   Focus Distance", "Distance to focus point."));
                     }
                     GUILayout.PropertyField(_depthOfFieldFocusSpeed, new GUIContent("   Focus Speed", "1=immediate focus on distance or target."));
                     GUILayout.PropertyField(_depthOfFieldFocalLength, new GUIContent("   Focal Length", "Focal length of the virtual lens."));
                     GUILayout.PropertyField(_depthOfFieldAperture, new GUIContent("   Aperture", "Diameter of the aperture (mm)."));
                     GUILayout.PropertyField(_depthOfFieldForegroundBlur, new GUIContent("   Foreground Blur", "Enables blur in front of focus object."));
                     if (_depthOfFieldForegroundBlur.boolValue)
                     {
                         GUILayout.PropertyField(_depthOfFieldForegroundDistance, new GUIContent("      Near Offset", "Distance from focus plane for foreground blur."));
                         if (_quality.intValue == (int)BEAUTIFY_QUALITY.BestQuality)
                         {
                             GUILayout.PropertyField(_depthOfFieldForegroundBlurHQ, new GUIContent("      High Quality", "Improves depth of field foreground blur."));
                         }
                     }
                     GUILayout.PropertyField(_depthOfFieldMaxBrightness, new GUIContent("   Clamp Brightness", "Clamps maximum pixel brightness to prevent out of range bright spots."));
                     GUILayout.PropertyField(_depthOfFieldBokeh, new GUIContent("   Bokeh", "Bright spots will be augmented in defocused areas."));
                     if (_depthOfFieldBokeh.boolValue)
                     {
                         GUILayout.PropertyField(_depthOfFieldBokehThreshold, new GUIContent("      Threshold", "Brightness threshold to be considered as 'bright' spot."));
                         GUILayout.PropertyField(_depthOfFieldBokehIntensity, new GUIContent("      Intensity", "Intensity multiplier for bright spots in defocused areas."));
                     }
                     GUILayout.PropertyField(_depthOfFieldDownsampling, new GUIContent("   Downsampling", "Reduces screen buffer size to improve performance."));
                     GUILayout.BeginHorizontal();
                     GUILayout.PropertyField(_depthOfFieldMaxSamples, new GUIContent("   Sample Count", "Determines the maximum number of samples to be gathered in the effect."));
                     GUILayout.Label("(" + ((_depthOfFieldMaxSamples.intValue - 1) * 2 + 1) + " samples)", GUILayout.Width(80));
                     GUILayout.EndHorizontal();

                     GUILayout.BeginHorizontal();
                     GUILayout.Label(new GUIContent("   Exclusion Mask", "Select which layers will always remain in focus."), GUILayout.Width(120));
                     if (testFeatureEnabled(isDoFTransp))
                     {
                         GUILayout.PropertyField(_depthOfFieldExclusionLayerMask, GUIContent.none);
                         if (_depthOfFieldExclusionLayerMask.intValue != 0)
                         {
                             GUILayout.EndHorizontal();
                             GUILayout.PropertyField(_depthOfFieldExclusionBias, new GUIContent("       Depth Bias", "Depth offset for the exclusion mask computation."));
                             GUILayout.PropertyField(_depthOfFieldExclusionLayerMaskDownsampling, new GUIContent("       Downsampling", "This value is added to the DoF downsampling factor for the exclusion mask creation. Increase to improve performance. "));
                             GUILayout.PropertyField(_depthOfFieldFilterMode, new GUIContent("       Filter Mode", "Texture filter mode."));
                         }
                         else
                         {
                             GUILayout.EndHorizontal();
                         }
                     }
                     else
                     {
                         GUILayout.EndHorizontal();
                     }

                     GUILayout.BeginHorizontal();
                     GUILayout.Label(new GUIContent("   Transparency", "Enable transparency support."), GUILayout.Width(120));
                     if (testFeatureEnabled(isDoFTransp))
                     {
                         if (_depthOfFieldExclusionLayerMask.intValue != 0)
                         {
                             GUI.enabled = false;
                             GUILayout.Toggle(true);
                             GUI.enabled = true;
                         }
                         else
                         {
                             GUILayout.PropertyField(_depthOfFieldTransparencySupport, GUIContent.none);
                         }
                         GUILayout.EndHorizontal();

                         if (_depthOfFieldTransparencySupport.boolValue || _depthOfFieldExclusionLayerMask.intValue != 0)
                         {
                             GUILayout.PropertyField(_depthOfFieldTransparencySupportDownsampling, new GUIContent("       Downsampling", "This value is added to the DoF downsampling factor for the transparency mask creation. Increase to improve performance."));
                         }
                     }
                     else
                     {
                         GUILayout.EndHorizontal();
                     }
                 }
                 else
                 {
                     GUILayout.EndHorizontal();
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             bool isEyeAdaptationEnabled = isShaderFeatureEnabled(Beautify.SKW_EYE_ADAPTATION, false);
             if (!isEyeAdaptationEnabled)
             {
                 if (_eyeAdaptation.boolValue)
                 {
                     _eyeAdaptation.boolValue = false;
                 }
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _eyeAdaptation.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandEASection = GUILayout.Foldout(expandEASection, new GUIContent("Eye Adaptation", "Enables eye adaptation effect. Simulates retina response to quick luminance changes in the scene."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isEyeAdaptationEnabled))
             {
                 GUILayout.PropertyField(_eyeAdaptation, GUIContent.none);
                 GUILayout.EndHorizontal();
                 if (expandEASection)
                 {
                     GUILayout.PropertyField(_eyeAdaptationMinExposure, new GUIContent("   Min Exposure"));
                     GUILayout.PropertyField(_eyeAdaptationMaxExposure, new GUIContent("   Max Exposure"));
                     GUILayout.PropertyField(_eyeAdaptationSpeedToDark, new GUIContent("   Dark Adapt Speed"));
                     GUILayout.PropertyField(_eyeAdaptationSpeedToLight, new GUIContent("   Light Adapt Speed"));
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             bool isPurkinjeEnabled = isShaderFeatureEnabled(Beautify.SKW_PURKINJE, false);
             if (!isPurkinjeEnabled)
             {
                 if (_purkinje.boolValue)
                 {
                     _purkinje.boolValue = false;
                 }
             }
             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _purkinje.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandPurkinjeSection = GUILayout.Foldout(expandPurkinjeSection, new GUIContent("Purkinje", "Simulates achromatic vision plus spectrum shift to blue in the dark."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isPurkinjeEnabled))
             {
                 GUILayout.PropertyField(_purkinje, GUIContent.none);
                 GUILayout.EndHorizontal();
                 if (expandPurkinjeSection)
                 {
                     GUILayout.PropertyField(_purkinjeAmount, new GUIContent("   Shift Amount", "Spectrum shift to blue. A value of zero will not shift colors and stay in grayscale."));
                     GUILayout.PropertyField(_purkinjeLuminanceThreshold, new GUIContent("   Threshold", "Increase this value to augment the purkinje effect (applies to higher luminance levels)."));
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.Separator();
             DrawLabel("Artistic Choices");

             bool isVignettingEnabled = isShaderFeatureEnabled(Beautify.SKW_VIGNETTING, false);
             if (!isVignettingEnabled)
             {
                 if (_vignetting.boolValue)
                 {
                     _vignetting.boolValue = false;
                 }
             }
             bool isFrameEnabled = isShaderFeatureEnabled(Beautify.SKW_FRAME, false);
             if (!isFrameEnabled)
             {
                 if (_frame.boolValue)
                 {
                     _frame.boolValue = false;
                 }
             }
             bool isOutlineEnabled = isShaderFeatureEnabled(Beautify.SKW_OUTLINE, false);
             if (!isOutlineEnabled)
             {
                 if (_outline.boolValue)
                 {
                     _outline.boolValue = false;
                 }
             }
             bool isNightVisionEnabled = isShaderFeatureEnabled(Beautify.SKW_NIGHT_VISION, false);
             if (!isNightVisionEnabled)
             {
                 if (_nightVision.boolValue)
                 {
                     _nightVision.boolValue = false;
                 }
             }
             bool isThermalVisionEnabled = isShaderFeatureEnabled(Beautify.SKW_THERMAL_VISION, false);
             if (!isThermalVisionEnabled)
             {
                 if (_thermalVision.boolValue)
                 {
                     _thermalVision.boolValue = false;
                 }
             }

             if (_vignetting.boolValue || _frame.boolValue || _outline.boolValue || _nightVision.boolValue || _thermalVision.boolValue)
             {
                 GUILayout.HelpBox("Customize the effects below using color picker. Alpha has special meaning depending on effect. Read the tooltip moving the mouse over the effect name.", MessageType.Info);
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _vignetting.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandVignettingSection = GUILayout.Foldout(expandVignettingSection, new GUIContent("Vignetting", "Enables colored vignetting effect. Color alpha specifies intensity of effect."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isVignettingEnabled))
             {
                 GUILayout.PropertyField(_vignetting, GUIContent.none);
                 GUILayout.EndHorizontal();
                 if (expandVignettingSection)
                 {
                     GUILayout.PropertyField(_vignettingColor, new GUIContent("   Color/Size", "The color for the vignetting effect. Alpha specifies intensity of effect."));
                     GUILayout.PropertyField(_vignettingFade, new GUIContent("   Fade Out", "Fade out effect to the vignette color."));
                     GUILayout.PropertyField(_vignettingBlink, new GUIContent("   Blink", "Manages blink effect for testing purposes. Use Beautify.instance.Blink to quickly produce a blink effect."));
                     GUILayout.PropertyField(_vignettingCircularShape, new GUIContent("   Circular Shape", "Ignores screen aspect ratio showing a circular shape."));
                     if (!_vignettingCircularShape.boolValue)
                     {
                         GUILayout.PropertyField(_vignettingAspectRatio, new GUIContent("   Aspect Ratio", "Custom aspect ratio."));
                     }
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _frame.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandFrameSection = GUILayout.Foldout(expandFrameSection, new GUIContent("Frame", "Enables colored frame effect. Color alpha specifies intensity of effect."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isFrameEnabled))
             {
                 GUILayout.PropertyField(_frame, GUIContent.none);
                 GUILayout.EndHorizontal();
                 if (expandFrameSection)
                 {
                     GUILayout.PropertyField(_frameColor, new GUIContent("   Color", "The color for the frame effect. Alpha specifies intensity of effect."));
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _outline.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandOutlineSection = GUILayout.Foldout(expandOutlineSection, new GUIContent("Outline", "Enables outline (edge detection) effect. Color alpha specifies edge detection threshold (reference values: 0.8 for depth, 0.35 for Sobel)."), labelStyle);
             GUILayout.EndHorizontal();
             if (testFeatureEnabled(isOutlineEnabled))
             {
                 GUILayout.PropertyField(_outline, GUIContent.none);
                 GUILayout.EndHorizontal();
                 if (expandOutlineSection)
                 {
                     GUILayout.PropertyField(_outlineColor, new GUIContent("   Color", "The color for the outline. Alpha specifies edge detection threshold."));
                     if (_outline.boolValue)
                     {
                         GUILayout.HelpBox("Choose between depth-based or color-based edge detection algorithm in Shader Options section.", MessageType.Info);
                     }
                 }
             }
             else
             {
                 GUILayout.EndHorizontal();
             }

             GUILayout.BeginHorizontal();
             labelStyle = _nightVision.boolValue ? labelBoldStyle : labelNormalStyle;
             GUILayout.Label(new GUIContent("Night Vision", "Enables night vision effect. Color alpha controls intensity. For a better result, enable Vignetting and set its color to (0,0,0,32)."), labelStyle, GUILayout.Width(120));
             if (testFeatureEnabled(isNightVisionEnabled))
             {
                 GUILayout.PropertyField(_nightVision, GUIContent.none);
                 if (_nightVision.boolValue)
                 {
                     GUILayout.Label(new GUIContent("Color", "The color for the night vision effect. Alpha controls intensity."), GUILayout.Width(40));
                     GUILayout.PropertyField(_nightVisionColor, GUIContent.none);
                 }
             }
             GUILayout.EndHorizontal();
             if (_lut.boolValue && _nightVision.boolValue)
                 GUILayout.HelpBox("Night Vision disabled by LUT.", MessageType.Info);

             GUILayout.BeginHorizontal();
             labelStyle = _thermalVision.boolValue ? labelBoldStyle : labelNormalStyle;
             GUILayout.Label(new GUIContent("Thermal Vision", "Enables thermal vision effect."), labelStyle, GUILayout.Width(120));
             if (testFeatureEnabled(isThermalVisionEnabled))
             {
                 GUILayout.PropertyField(_thermalVision, GUIContent.none);
             }
             GUILayout.EndHorizontal();
             if (_lut.boolValue && _thermalVision.boolValue)
                 GUILayout.HelpBox("Thermal Vision disabled by LUT.", MessageType.Info);

             GUILayout.BeginHorizontal();
             GUILayout.BeginHorizontal(GUILayout.Width(120));
             labelStyle = _blur.boolValue ? sectionHeaderBoldStyle : sectionHeaderNormalStyle;
             expandBlurSection = GUILayout.Foldout(expandBlurSection, new GUIContent("Blur", "Enables final blur effect."), labelStyle);
             GUILayout.EndHorizontal();
             GUILayout.PropertyField(_blur, GUIContent.none);
             GUILayout.EndHorizontal();
             if (expandBlurSection)
             {
                 GUILayout.PropertyField(_blurIntensity, new GUIContent("   Intensity"));
             }

             GUILayout.PropertyField(_pixelateAmount, new GUIContent("Pixelate", "Creates a retro/pixelated effect."));
             GUILayout.PropertyField(_pixelateDownscale, new GUIContent("   Downscale", "If enabled, makes the camera render to a smaller render target. This option increases performance while producing the pixelation effect. If disabled, the pixelation effect is applied to the normal framebuffer."));


             if (serializedObject.ApplyModifiedProperties() || (Event.current.type == EventType.ValidateCommand &&
                             Event.current.commandName == "UndoRedoPerformed" || GUI.changed || _effect.isDirty))
             {
                 if (qualityChanged)
                 {
                     _effect.UpdateQualitySettings();
                 }
                 if (!generalSettingsChanged)
                 {
                     _effect.preset = BEAUTIFY_PRESET.Custom;
                 }

                 _effect.UpdateMaterialPropertiesNow();
                 _effect.isDirty = false;
                 EditorUtility.SetDirty(target);
             }*/
        }

        public void EnableGUI()
        {
            if (openGUI == true) return;
            titleColor = new Color(0.52f, 0.66f, 0.9f);
            blackBack = new GUIStyle();
            blackBack.normal.background = MakeTex(4, 4, Color.black);
            ScanKeywords();
            openGUI = true;
        }

        public void DisableGUI()
        {
            if (openGUI == false) return;
            openGUI = false;
        }

        const string PRAGMA_COMMENT_MARK = "// Edited by Shader Control: ";
        const string PRAGMA_DISABLED_MARK = "// Disabled by Shader Control: ";
        const string PRAGMA_MULTICOMPILE = "#pragma multi_compile ";
        const string PRAGMA_UNDERSCORE = "__ ";
        static GUIStyle titleLabelStyle, labelBoldStyle, labelNormalStyle, sectionHeaderBoldStyle, sectionHeaderNormalStyle, sectionHeaderIndentedStyle;
        static GUIStyle buttonNormalStyle, buttonPressedStyle, blackBack;
        static Color titleColor;
        bool expandSharpenSection, expandBloomSection, expandAFSection;
        bool expandSFSection, expandSFCoronaRays1Section, expandSFCoronaRays2Section, expandSFGhosts1Section, expandSFGhosts2Section, expandSFGhosts3Section, expandSFGhosts4Section, expandSFHaloSection;
        bool expandDirtSection, expandDoFSection;
        bool expandEASection, expandPurkinjeSection, expandVignettingSection, expandDitherSection, expandFrameSection;
        bool expandOutlineSection, expandLUTSection, expandBlurSection;
        //  bool toggleOptimizeBuild;
        // BeautifySAdvancedOptionsInfo shaderAdvancedOptionsInfo;

        GUIStyle foldoutBold, foldoutNormal, foldOutIndented;
        List<BeautifySInfo> shaders;
        bool layer1, layer2, layer3, layer4, layer5, layer6;
        bool profileChanges;

        void DrawLabel(string s)
        {
            if (titleLabelStyle == null)
            {
                GUIStyle skurikenModuleTitleStyle = "ShurikenModuleTitle";
                titleLabelStyle = new GUIStyle(skurikenModuleTitleStyle);
                titleLabelStyle.contentOffset = new Vector2(5f, -2f);
                titleLabelStyle.normal.textColor = titleColor;
                titleLabelStyle.fixedHeight = 22;
                titleLabelStyle.fontStyle = FontStyle.Bold;
            }

            GUILayout.Label(s, titleLabelStyle);
        }

        Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            TextureFormat tf = SystemInfo.SupportsTextureFormat(TextureFormat.RGBAFloat) ? TextureFormat.RGBAFloat : TextureFormat.RGBA32;
            Texture2D result = new Texture2D(width, height, tf, false);
            result.hideFlags = HideFlags.DontSave;
            result.SetPixels(pix);
            result.Apply();

            return result;
        }

        void ScanKeywords()
        {
            if (shaders == null)
                shaders = new List<BeautifySInfo>();
            else
                shaders.Clear();
            //   string[] guids = AssetDatabase.FindAssets("Beautify t:Shader");

            //   string guid = guids[0];
            //  string path = AssetDatabase.GUIDToAssetPath(guid);
            BeautifySInfo shader = new BeautifySInfo();
            //shader.path = path;
            shader.name = "Beautify";// Path.GetFileNameWithoutExtension(path);
            ScanShader(shader);
            if (shader.keywords.Count > 0)
            {
                shaders.Add(shader);
            }
        }

        void ScanShader(BeautifySInfo shader)
        {
            // Inits shader
            shader.passes.Clear();
            shader.keywords.Clear();
            shader.pendingChanges = false;
            shader.editedByShaderControl = false;

            // Reads shader
            string temp = @"
Shader \'Beautify/Beautify\' {\n
	Properties {\n
		_MainTex (\'Base (RGB)\', 2D) = \'white\' {}\n
		_OverlayTex (\'Overlay (RGB)\', 2D) = \'black\' {}\n
		//_DoFTex(\'DoF (RGBA)\', 2D) = \'black\' {}\n
		_DoFTex(\'DoF (RGBA)\', any) = \'black\' {}\n
		_Sharpen (\'Sharpen Data\', Vector) = (2.5, 0.035, 0.5)\n
		_ColorBoost (\'Color Boost Data\', Vector) = (1.1, 1.1, 0.08, 0)\n
		_Dither (\'Dither Data\', Vector) = (5, 0, 0, 1.0)\n
		_FXColor (\'FXColor Color\', Color) = (1,1,1,0)\n
		_TintColor (\'Tint Color Color\', Color) = (1,1,1,0)\n
		_Vignetting (\'Vignetting\', Color) = (0.3,0.3,0.3,0.05)\n
		_VignettingAspectRatio (\'Vignetting Aspect Ratio\', Float) = 1.0\n
		_VignettingMask (\'Mask Texture (A)\', 2D) = \'white\' {}\n
		_Frame(\'Frame Data\', Vector) = (50,50,50,0)\n
		_FrameMask (\'Mask Texture (A)\', 2D) = \'white\' {}\n
		_Outline(\'Outline\', Color) = (0,0,0,0.8)\n
		_Dirt(\'Dirt Data\', Vector) = (0.5,0.5,0.5,0.5)\n
		_Bloom(\'Bloom Data\', Vector) = (0.5,0,0)\n
		_BloomTex(\'BloomTex (RGBA)\', any) = \'black\' {}\n
		//_BloomTex(\'BloomTex (RGBA)\', 2D) = \'black\' {}\n
		_BloomWeights(\'Bloom Weights\', Vector) = (0.35,0.55,0.7,0.8)\n
		_BloomWeights2(\'Bloom Weights 2\', Vector) = (0.35,0.55,0.7,0.8)\n
		_ScreenLum(\'Luminance Tex (RGBA)\', any) = \'black\' {}\n
		//_ScreenLum(\'Luminance Tex (RGBA)\', 2D) = \'black\' {}\n
		_CompareParams(\'Compare Params\', Vector) = (0.785398175, 0.001, 0, 0)\n
		_AFTint (\'Anamorphic Flares Tint\', Color) = (1,1,1,0.5)\n
		_BokehData(\'Bokeh Data\', Vector) = (10,1,0,1)\n
		_DepthTexture(\'Depth (RGBA)\', 2D) = \'black\' {}\n
		_DofExclusionTexture(\'DoF Exclusion (R)\', 2D) = \'white\' {}\n
		_BokehData2(\'Bokeh Data 2\', Vector) = (1000.0, 4, 0, 0)\n
		_BokehData3(\'Bokeh Data 3\', Float) = 1000.0\n
		_EyeAdaptation(\'Eye Adaptation Data\', Vector) = (0.1, 2, 0.7, 1)\n
		_Purkinje (\'Purkinje Data\', Vector) = (1.0, 0.15, 0)\n
		_BloomDepthTreshold(\'Bloom Depth Threshold\', Float) = 1.0\n
		_FlareTex(\'Sun Flare texture\', 2D) = \'black\' {}\n
		_SunPos(\'SunFlares Sun Screen Position\', Vector) = (0.5, 0.5, 0, 0)\n
		_SunData(\'SunFlares Sun Data\', Vector) = (0.1, 0.05, 3.5, 0.13)\n
		_SunCoronaRays1(\'SunFlares Corona Rays 1 Data\', Vector) = (0.02, 12, 0.001, 0)\n
		_SunCoronaRays2(\'SunFlares Corona Rays 2 Data\', Vector) = (0.05, 12, 0.1, 0)\n
		_SunGhosts1(\'SunFlares Ghosts 1 Data\', Vector) = (0, 0.03, 0.6, 0.06)\n
		_SunGhosts2(\'SunFlares Ghosts 2 Data\', Vector) = (0, 0.1, 0.2, 0.03)\n
		_SunGhosts3(\'SunFlares Ghosts 3 Data\', Vector) = (0, 0.25, 0.4, 0.025)\n
		_SunGhosts4(\'SunFlares Ghosts 4 Data\', Vector) = (0, 0.5, 0.3, 0.04)\n
		_SunTint(\'Sun Flare Tint Color\', Color) = (1,1,1)\n
		_SunHalo(\'SunFlares Halo Data\', Vector) = (0.22, 15.1415, 1.0)\n
		_CompareTex (\'Compare Image (RGB)\', any) = \'black\' {}\n
//		_CompareTex (\'Compare Image (RGB)\', 2D) = \'black\' {}\n
		_BlurScale (\'Blur Scale\', Float) = 1.0\n
		_LUTTex (\'Lut Texture (RGB)\', 2D) = \'white\' {}\n
	}\n
\n
Subshader {	\n

  Pass { // 0\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertCompare\n
      #pragma fragment fragCompare\n
      #pragma target 3.0\n
      #include \'Beautify.cginc\'\n
      ENDCG\n
  }\n
 \n
  Pass { // 1\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragBeautify\n
      #pragma target 3.0\n
#pragma multi_compile __ BEAUTIFY_DALTONIZE BEAUTIFY_LUT BEAUTIFY_NIGHT_VISION BEAUTIFY_THERMAL_VISION\n
#pragma multi_compile __ BEAUTIFY_DEPTH_OF_FIELD BEAUTIFY_DEPTH_OF_FIELD_TRANSPARENT\n
#pragma multi_compile __ BEAUTIFY_OUTLINE\n
#pragma multi_compile __ BEAUTIFY_DIRT\n
#pragma multi_compile __ BEAUTIFY_BLOOM\n
#pragma multi_compile __ BEAUTIFY_EYE_ADAPTATION\n
#pragma multi_compile __ BEAUTIFY_TONEMAP_ACES\n
#pragma multi_compile __ BEAUTIFY_PURKINJE\n
// Edited by Shader Control: #pragma multi_compile __ BEAUTIFY_VIGNETTING BEAUTIFY_VIGNETTING_MASK\n
#pragma multi_compile __ BEAUTIFY_VIGNETTING \n
// Edited by Shader Control: #pragma multi_compile __ BEAUTIFY_FRAME BEAUTIFY_FRAME_MASK\n
#pragma multi_compile __ BEAUTIFY_FRAME \n
#pragma multi_compile __ UNITY_COLORSPACE_GAMMA\n
\n
\n
      #include \'Beautify.cginc\'\n
      ENDCG\n
  }\n
  \n
  Pass { // 2\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertLum\n
      #pragma fragment fragLum\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
#pragma multi_compile __ BEAUTIFY_BLOOM_USE_DEPTH\n
#pragma multi_compile __ BEAUTIFY_BLOOM_USE_LAYER\n
#pragma multi_compile __ UNITY_COLORSPACE_GAMMA\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }  \n
  \n
  \n
  Pass { // 3\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragDebugBloom\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }   \n
  \n
  Pass { // 4\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertBlurH\n
      #pragma fragment fragBlur\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
	  #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }    \n
  
      
  Pass { // 5\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertBlurV\n
      #pragma fragment fragBlur\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }    \n
\n
            \n
  Pass { // 6\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragBloomCompose\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }   \n
  \n
  Pass { // 7\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertCross\n
      #pragma fragment fragResample\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }   \n
    \n
  Pass { // 8\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  Blend One One\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertCross\n
      #pragma fragment fragResample\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  } \n
\n
\n
  Pass { // 9\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertCrossLum\n
      #pragma fragment fragLumAntiflicker\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
#pragma multi_compile __ BEAUTIFY_BLOOM_USE_DEPTH\n
#pragma multi_compile __ BEAUTIFY_BLOOM_USE_LAYER\n
#pragma multi_compile __ UNITY_COLORSPACE_GAMMA\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  }  \n
\n
  Pass { // 10\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  Blend One One\n
\n
      CGPROGRAM\n
      #pragma vertex vertCross\n
      #pragma fragment fragResampleAF\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  } \n

  Pass { // 11\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  Blend One One\n
\n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragCopy\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  } \n
\n
  Pass { // 12 DoF CoC\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n

      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragCoC\n
      #pragma target 3.0\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
#pragma multi_compile __ BEAUTIFY_DEPTH_OF_FIELD_TRANSPARENT\n
#pragma multi_compile __ UNITY_COLORSPACE_GAMMA\n
      #include \'BeautifyDoF.cginc\'\n
      ENDCG\n
  } \n
 
  Pass { // 13 DoF CoC Debug\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
\n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragCoCDebug\n
      #pragma target 3.0\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
#pragma multi_compile __ BEAUTIFY_DEPTH_OF_FIELD_TRANSPARENT\n
      #include \'BeautifyDoF.cginc\'\n
      ENDCG\n
  } \n
 \n
  Pass { // 14 DoF Blur\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
\n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragBlur\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyDoF.cginc\'\n
      ENDCG\n
  }    \n
  \n
 Pass { // 15 Compute Screen Lum\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragScreenLum\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
#pragma multi_compile __ UNITY_COLORSPACE_GAMMA\n
      #include \'BeautifyEA.cginc\'\n
      ENDCG\n
  }      \n
  \n
  Pass { // 16 Reduce Screen Lum\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vertCross\n
      #pragma fragment fragReduceScreenLum\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyEA.cginc\'\n
      ENDCG\n
  }  \n
  \n
  Pass { // 17 Blend Screen Lum\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  Blend SrcAlpha OneMinusSrcAlpha\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragBlendScreenLum\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyEA.cginc\'\n
      ENDCG\n
  }    \n
      \n
  Pass { // 18 Simple Blend\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
//	  Blend SrcAlpha OneMinusSrcAlpha\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragBlend\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyEA.cginc\'\n
      ENDCG\n
  }  \n
 \n
  Pass { // 19 DoF Blur wo/Bokeh\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragBlurNoBokeh\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyDoF.cginc\'\n
      ENDCG\n
  }    \n
  \n
  Pass { // 20 Sun Flares\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragSF\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifySF.cginc\'\n
      ENDCG\n
  }\n
  \n
    Pass { // 21 Sun Flares Additive\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
	  \n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragSFAdditive\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifySF.cginc\'\n
      ENDCG\n
  }\n
\n
  Pass { // 22  Raw copy used in single blits with Single Pass Instanced\n
	  ZTest Always Cull Off ZWrite Off\n
	  Fog { Mode Off }\n
\n
      CGPROGRAM\n
      #pragma vertex vert\n
      #pragma fragment fragCopy\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyLum.cginc\'\n
      ENDCG\n
  } \n
\n
\n
Pass { // 23 DoF Blur CoC\n
      ZTest Always Cull Off ZWrite Off\n
      Fog { Mode Off }\n
      \n
      CGPROGRAM\n
      #pragma vertex vertBlurH\n
      #pragma fragment fragBlurCoC\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyDoF.cginc\'\n
      ENDCG\n
  }    \n
\n
Pass { // 24 DoF Blur CoC\n
      ZTest Always Cull Off ZWrite Off\n
      Fog { Mode Off }\n
      \n
      CGPROGRAM\n
      #pragma vertex vertBlurV\n
      #pragma fragment fragBlurCoC\n
      #pragma fragmentoption ARB_precision_hint_fastest\n
      #pragma target 3.0\n
      #include \'BeautifyDoF.cginc\'\n
      ENDCG\n
  }    \n
  \n
 }\n
FallBack Off\n
}\n";
            string[] shaderLines = temp.Split('\n');
            string[] separator = new string[] { " " };
            SCShaderPass currentPass = new SCShaderPass();
            SCShaderPass basePass = null;
            int pragmaControl = 0;
            int pass = -1;
            SCKeywordLine keywordLine = new SCKeywordLine();
            for (int k = 0; k < shaderLines.Length; k++)
            {
                string line = shaderLines[k].Trim();
                if (line.Length == 0)
                    continue;
                string lineUPPER = line.ToUpper();
                if (lineUPPER.Equals("PASS") || lineUPPER.StartsWith("PASS "))
                {
                    if (pass >= 0)
                    {
                        currentPass.pass = pass;
                        if (basePass != null)
                            currentPass.Add(basePass.keywordLines);
                        shader.Add(currentPass);
                    }
                    else if (currentPass.keywordCount > 0)
                    {
                        basePass = currentPass;
                    }
                    currentPass = new SCShaderPass();
                    pass++;
                    continue;
                }
                int j = line.IndexOf(PRAGMA_COMMENT_MARK);
                if (j >= 0)
                {
                    pragmaControl = 1;
                }
                else
                {
                    j = line.IndexOf(PRAGMA_DISABLED_MARK);
                    if (j >= 0)
                        pragmaControl = 3;
                }
                j = line.IndexOf(PRAGMA_MULTICOMPILE);
                if (j >= 0)
                {
                    if (pragmaControl != 2)
                        keywordLine = new SCKeywordLine();
                    string[] kk = line.Substring(j + 22).Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    // Sanitize keywords
                    for (int i = 0; i < kk.Length; i++)
                        kk[i] = kk[i].Trim();
                    // Act on keywords
                    switch (pragmaControl)
                    {
                        case 1: // Edited by Shader Control line
                            shader.editedByShaderControl = true;
                            // Add original keywords to current line
                            for (int s = 0; s < kk.Length; s++)
                            {
                                keywordLine.Add(shader.GetKeyword(kk[s]));
                            }
                            pragmaControl = 2;
                            break;
                        case 2:
                            // check enabled keywords
                            keywordLine.DisableKeywords();
                            for (int s = 0; s < kk.Length; s++)
                            {
                                SCKeyword keyword = keywordLine.GetKeyword(kk[s]);
                                if (keyword != null)
                                    keyword.enabled = true;
                            }
                            currentPass.Add(keywordLine);
                            pragmaControl = 0;
                            break;
                        case 3: // disabled by Shader Control line
                            shader.editedByShaderControl = true;
                            // Add original keywords to current line
                            for (int s = 0; s < kk.Length; s++)
                            {
                                SCKeyword keyword = shader.GetKeyword(kk[s]);
                                keyword.enabled = false;
                                keywordLine.Add(keyword);
                            }
                            currentPass.Add(keywordLine);
                            pragmaControl = 0;
                            break;
                        case 0:
                            // Add keywords to current line
                            for (int s = 0; s < kk.Length; s++)
                            {
                                keywordLine.Add(shader.GetKeyword(kk[s]));
                            }
                            currentPass.Add(keywordLine);
                            break;
                    }
                }
            }
            currentPass.pass = Mathf.Max(pass, 0);
            if (basePass != null)
                currentPass.Add(basePass.keywordLines);
            shader.Add(currentPass);
        }


        bool isShaderFeatureEnabled(string name, bool drawLabel = true)
        {
            if (shaders.Count == 0)
                return false;
            SCKeyword keyword = shaders[0].GetKeyword(name);
            if (drawLabel)
            {
                return testFeatureEnabled(keyword.enabled);
            }
            return keyword.enabled;
        }

        bool testFeatureEnabled(bool value)
        {
            if (!value)
            {
                GUILayout.Label("(feature disabled in build options)");
                return false;
            }
            return true;
        }


        #endregion
    }

}