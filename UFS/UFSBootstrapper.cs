﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BDArmory.Core.Services;
using UnityEngine;
using BDArmory.Core;

namespace UFS
{
    [KSPAddon(KSPAddon.Startup.AllGameScenes, false)]
    public class UFSBootstrapper:MonoBehaviour
    {
        private void Awake()
        {
            Dependencies.Register<DamageService, UFSModuleDamageService>();
        }
    }
}
