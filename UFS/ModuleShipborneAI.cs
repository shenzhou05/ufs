﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using KSP;
using KSP.Localization;

namespace UFS
{
    public class ModuleShipborneAI : PartModule
    {
        [KSPField(isPersistant = true, guiActive = true, guiActiveEditor = true, guiName = "AI Status")]
        public bool AIEabled = false;

        #region User interface, action group name, buttons

        /*TODO:
         * Text localization support
         */

        [KSPEvent(name = "Activate", guiName = "Activate Shipborne AI", active = true, guiActive = true, isPersistent = true, guiActiveEditor = true)]
        public void Activate()
        {
            this.AIEabled = true;
            Events["Activate"].guiActive = false;
            Events["Deactivate"].guiActive = true;
            Events["Activate"].guiActiveEditor= false;
            Events["Deactivate"].guiActiveEditor = true;

            ScreenMessages.PostScreenMessage("<color=#59ff5cff>[Shipborne AI] Shipborne AI On!</color>", 1f, ScreenMessageStyle.UPPER_CENTER);
        }

        [KSPEvent(name = "Deactivate", guiName = "Deactivate Shipborne AI", active = true, guiActive = false, isPersistent = true, guiActiveEditor = false)]
        public void Deactivate()
        {
            this.AIEabled = false;
            Events["Deactivate"].guiActive = false;
            Events["Activate"].guiActive = true;
            Events["Deactivate"].guiActiveEditor = false;
            Events["Activate"].guiActiveEditor = true;

            ScreenMessages.PostScreenMessage("<color=#59ff5cff>[Shipborne AI] Shipborne AI Off!</color>", 1f, ScreenMessageStyle.UPPER_CENTER);
        }

        [KSPAction("Activate Shipborne AI", isPersistent = true)]
        public void ActivateAction(KSPActionParam param)
        {
            Activate();
        }

        [KSPAction("Deactivate Shipborne AI", isPersistent = true)]
        public void DeactivateAction(KSPActionParam param)
        {
            Deactivate();
        }

        [KSPAction("Toggle Shipborne AI", isPersistent = true)]
        public void ToggleAction(KSPActionParam param)
        {
            if (AIEabled)
            {
                Deactivate();
            }
            else
            {
                Activate();
            }
        }

        public override string GetModuleDisplayName()//editor part list RMB additional infomation header
        {
            return "K.I.R.K.";
        }

        public override string GetInfo() //editor part list RMB additional infomation content
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(Environment.NewLine);
            stringBuilder.AppendLine("<b>Knowledge-Integrated Reserve Kit</b>");
            stringBuilder.AppendLine("<i>This part has K.I.R.K. module onboard, bon voyage!</i>");

            return stringBuilder.ToString();
        }

        #endregion
    }
}
