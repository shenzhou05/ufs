﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using KSP;
using BDArmory.Core.Module;
using BDArmory.Core.Events;
using BDArmory.Core.Enum;
using UnityEngine.Rendering;

namespace UFS
{
    public class ModuleShieldGenerator : PartModule
    {
        [KSPField(isPersistant = true)]
        public string shieldName = "shield";
        [KSPField(isPersistant = true)]
        public float clearanceRatio = 5;
        [KSPField(isPersistant = true)]
        public float openShieldDispayTime = 1;
        [KSPField(isPersistant = true)]
        public float closeShieldDisplayTime = 1;
        [KSPField]
        public string start_sound = "";
        [KSPField]
        public string stop_sound = "";
        [KSPField]
        public string damaged_sound = "";
        [KSPField]
        public string collided_sound_loop = "";

        //跟模型的碰撞网相关
        [KSPField(isPersistant = true)]
        public bool _switchShield = false;
        [KSPField(isPersistant = true)]
        public bool _switchCharge = false;
        [KSPField(isPersistant = true, guiActive = true, guiActiveEditor = true, guiName = "Shield Max Value")]
        public float _shieldMaxValue = 0;
        [KSPField(isPersistant = true)]
        public int _shieldBaseValue = 0;
        [KSPField(isPersistant = true, guiActive = true, guiActiveEditor = false, guiName = "Shield Current Value")]
        public float _shieldCurrentValue = 0;
        [UI_ProgressBar(affectSymCounterparts = UI_Scene.None, controlEnabled = false, scene = UI_Scene.Flight, maxValue = 100, minValue = 0, requireFullControl = false)]
        [KSPField(isPersistant = true, guiActive = true, guiActiveEditor = false, guiName = "Shield Percent")]
        public float _ShieldCompletenessPercent = 100f;
        [UI_FloatRange(minValue = 20, maxValue = 200)]
        [KSPField(isPersistant = true, guiActive = true, guiActiveEditor = true, guiName = "Launch Speed Threshold")]
        public float _launchSpeedThreshold = 100;
        [UI_FloatRange(minValue = 5, maxValue = 50)]
        [KSPField(isPersistant = true, guiActive = true, guiActiveEditor = true, guiName = "Launch Mass Threshold")]
        public float _launchMassThreshold = 25;
        [KSPField(isPersistant = true)]
        public bool _isSingle = false;
        //局部缩放
        [KSPField(isPersistant = true)]
        public Vector3 _localSize = new Vector3();
        //局部旋转
        [KSPField(isPersistant = true)]
        public Vector3 _localEuler = new Vector3();
        //局部坐标
        [KSPField(isPersistant = true)]
        public Vector3 _localCenter = new Vector3();
        [KSPField(isPersistant = true)]
        public float _shieldDisplayIntensity = 0;

        public AudioClip colliderShield;
        public bool isHurting = false;

        private bool doOnce = true;
        private bool doOnce2 = true;
        private ShieldCollider shieldCollider;
        private AudioSource audioSource;
        private AudioClip openShield;
        private AudioClip closeShield;
        private AudioClip attackShield;
        private EffectBase effect;

        private float ShieldTransparency
        {
            get
            {
                return this._shieldDisplayIntensity;
            }
            set
            {
                if (value > 1) value = 1;
                if (value < 0) value = 0;
                this._shieldDisplayIntensity = value;
            }
        }

        //请使用该函数的invoke对_switchShield进行控制
        [KSPEvent(name = "Activate", guiName = "Shield on", active = true, guiActive = true, isPersistent = true, guiActiveEditor = false)]
        public void Activate()
        {
            if (this._ShieldCompletenessPercent < 80)
            {
                ScreenMessages.PostScreenMessage("<color=#ff0000ff>[ModuleShieldGenerator] Shield Percent should be charged up to 80%!</color>", 5f, ScreenMessageStyle.UPPER_CENTER);
                return;
            }
            this._switchShield = true;
            Events["Activate"].guiActive = false;
            Events["Deactivate"].guiActive = true;
            if (audioSource)
            {
                audioSource.clip = openShield;
                audioSource.PlayOneShot(openShield);
            }
        }

        //正常关闭护盾
        [KSPEvent(name = "Deactivate", guiName = "Shield off", active = true, guiActive = false, isPersistent = true, guiActiveEditor = false)]
        public void Deactivate()
        {
            this._switchShield = false;
            Events["Deactivate"].guiActive = false;
            Events["Activate"].guiActive = true;
            if (audioSource)
            {
                audioSource.clip = closeShield;
                audioSource.PlayOneShot(closeShield);
            }
        }

        [KSPAction("Activate Shield", isPersistent = true)]
        public void ActivateShieldAction(KSPActionParam param)
        {
            Activate();
        }

        [KSPAction("Deactivate Shield", isPersistent = true)]
        public void DeactivateShieldAction(KSPActionParam param)
        {
            Deactivate();
        }

        /* [KSPEvent(name = "Charge_Start", guiName = "Charge on", active = true, guiActive = true, isPersistent = true, guiActiveEditor = false)]
         public void Charge_Start()
         {
             this._switchCharge = true;
             Events["Charge_Start"].guiActive = false;
             Events["Charge_Close"].guiActive = true;
         }

         [KSPEvent(name = "Charge_Close", guiName = "Charge off", active = true, guiActive = false, isPersistent = true, guiActiveEditor = false)]
         public void Charge_Close()
         {
             this._switchCharge = false;
             Events["Charge_Close"].guiActive = false;
             Events["Charge_Start"].guiActive = true;
         }*/

        public override void OnStart(StartState state)
        {
            base.OnStart(state);
            if (HighLogic.LoadedScene == GameScenes.EDITOR)
            {
                if (ModuleShieldGeneratorController.shield == null)
                {
                    ModuleShieldGeneratorController.shield = this;
                    _isSingle = true;
                }
                else
                {
                    _isSingle = false;
                    //警告
                    Debug.LogError("[ModuleShieldGenerator] Already has shield generator module!");
                    ScreenMessages.PostScreenMessage("<color=#ff0000ff>[ModuleShieldGenerator] Already has shield generator module!</color>", 10f, ScreenMessageStyle.UPPER_CENTER);
                }
                this._switchShield = false;
                this._switchCharge = false;
                this._shieldDisplayIntensity = 0;
                this._shieldMaxValue = 0;
                this._shieldBaseValue = 0;
                this._ShieldCompletenessPercent = 100f;
                Events["Activate"].guiActive = true;
                Events["Deactivate"].guiActive = false;
               // Events["Charge_Start"].guiActive = true;
               // Events["Charge_Close"].guiActive = false;
            }
            if (HighLogic.LoadedScene == GameScenes.FLIGHT)
            {
                ModuleShieldGeneratorController.shield = null;
                if (!_isSingle)
                {
                    //警告+爆炸
                    Debug.LogError("[ModuleShieldGenerator] Already has shield generator module!");
                    ScreenMessages.PostScreenMessage("<color=#ff0000ff>[ModuleShieldGenerator] Already has shield generator module!</color>", 10f, ScreenMessageStyle.UPPER_CENTER);
                    this.part.explode();
                    this.part.decouple(100);
                    DestroyImmediate(this.part);
                    return;
                }
            }
        }

        private void InitializeAudioSource()
        {
            var shield = this.part.FindModelTransform(shieldName);
            if (shield == null)
            {
                Debug.LogError("[ModuleShieldGenerator] Wrong shield name!");
                return;
            }
            audioSource = shield.gameObject.AddComponent<AudioSource>();
            audioSource.bypassListenerEffects = true;
            audioSource.minDistance = 100f;
            audioSource.maxDistance = 1000f;
            audioSource.priority = 1;
            audioSource.dopplerLevel = 0;
            audioSource.spatialBlend = 1;
            audioSource.loop = false;
            audioSource.time = 0;
            audioSource.volume = 1;
            openShield = GameDatabase.Instance.GetAudioClip(start_sound);
            closeShield = GameDatabase.Instance.GetAudioClip(stop_sound);
            attackShield = GameDatabase.Instance.GetAudioClip(damaged_sound);
            colliderShield = GameDatabase.Instance.GetAudioClip(collided_sound_loop);
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            if (HighLogic.LoadedScene == GameScenes.FLIGHT && _isSingle == true)
            {
                ///测试用
               /* if (Input.GetKeyDown(KeyCode.KeypadPlus))
                {
                    ScriptManager.GetInstance().Start();
                    // this._ShieldCompletenessPercent = 100f;
                }*/

                var shield = this.part.FindModelTransform(shieldName);
                if (shield == null)
                {
                    Debug.LogError("[ModuleShieldGenerator] Wrong shield name!");
                    return;
                }
                if (doOnce)
                {
                    shieldCollider = shield.GetComponent<ShieldCollider>();
                    audioSource = shield.GetComponent<AudioSource>();
                    if (shieldCollider == null)
                    {
                        shieldCollider = shield.gameObject.AddComponent<ShieldCollider>();
                    }
                    if (audioSource == null)
                    {
                        InitializeAudioSource();
                    }
                    this._switchCharge = true;
                    shield.GetComponent<Collider>().isTrigger = true;
                    shield.localScale = this._localSize;
                    shield.localEulerAngles = this._localEuler;
                    shield.localPosition = this._localCenter;
                    doOnce = false;
                }
                //更新shieldCollider组件
                if (shieldCollider)
                {
                    shieldCollider.enabled = true;
                    shieldCollider.shieldPart = this.part;
                    shieldCollider.startCheck = true;
                    shieldCollider.maxMass = _launchMassThreshold;
                    shieldCollider.maxSpeed = _launchSpeedThreshold;
                    shieldCollider.collisionDamage = 5000f;
                    shieldCollider.explosionDamage = 5f;
                }
                if (audioSource)
                    audioSource.enabled = true;

                if (this._switchCharge)
                {
                    if (this._ShieldCompletenessPercent < 100f)
                    {
                        int hasShieldFieldCount = 0;
                        double availableShieldField = 0;
                        foreach (var _part in this.vessel.Parts)
                        {
                            var resource = _part.Resources.Get("ShieldField");
                            if (resource != null &&
                            resource.amount > 0 &&
                             resource.isTweakable)
                            {
                                hasShieldFieldCount++;
                                availableShieldField += resource.amount;
                            }
                        }
                        if (availableShieldField > 0)
                        {
                            double neededResource = Time.fixedDeltaTime * Planetarium.TimeScale * 200 * Mathf.Log(hasShieldFieldCount + 1, 2);
                            double returnResource = this.part.RequestResource("ShieldField", neededResource);
                            float tempValue = 0f;
                            if (returnResource != neededResource || this.isHurting)
                            {
                                //非安静状态
                                tempValue = this._shieldCurrentValue + Time.fixedDeltaTime * (float)Planetarium.TimeScale * 500 * Mathf.Log(hasShieldFieldCount + 1, 2);
                            }
                            else
                            {
                                tempValue = this._shieldCurrentValue + Time.fixedDeltaTime * (float)Planetarium.TimeScale * 1000 * Mathf.Log(hasShieldFieldCount + 1, 2);
                            }
                            tempValue /= this._shieldMaxValue;
                            if (tempValue > 1f) tempValue = 1f;
                            if (tempValue < 0f) tempValue = 0f;
                            this._ShieldCompletenessPercent = tempValue * 100f;
                        }
                    }
                }

                if (!this._switchShield)
                {
                    // if (shieldCollider)
                    //     shieldCollider.enabled = false;
                    shield.GetComponent<Collider>().enabled = false;
                    this.ShieldTransparency -= Time.fixedDeltaTime * (float)Planetarium.TimeScale / this.openShieldDispayTime;
                }
                else
                {
                    this.ShieldTransparency += Time.fixedDeltaTime * (float)Planetarium.TimeScale / this.closeShieldDisplayTime;
                    shield.GetComponent<Collider>().enabled = true;
                    //if (shieldCollider)
                    //    shieldCollider.enabled = true;
                    this.part.RequestResource("ShieldField", Time.fixedDeltaTime * Planetarium.TimeScale * 50);
                    this.part.RequestResource("ElectricCharge", Time.fixedDeltaTime * Planetarium.TimeScale * 25);
                    if (GetAvailableResource("ShieldField") <= 0 || GetAvailableResource("ElectricCharge") <= 0)
                    {
                        this.Events["Deactivate"].Invoke();
                        ScreenMessages.PostScreenMessage("<color=#ff0000ff>[ModuleShieldGenerator] Low power!</color>", 5f, ScreenMessageStyle.UPPER_CENTER);
                    }
                }

                var mr = shield.GetComponent<MeshRenderer>();
                mr.enabled = true;
                mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                Material materialDefault = mr.material;
                if (doOnce2)
                {
                    if (ResourcesController.ufsAB != null)
                    {
                        materialDefault = ResourcesController.ufsAB.LoadAsset("UFSShield") as Material;
                        doOnce2 = false;
                    }
                }

                //护盾值
                double avgHitPoint = 0;
                foreach (var part in this.vessel.Parts)
                {
                    avgHitPoint += part.Modules.GetModule<HitpointTracker>().Hitpoints;

                }
                if (this.vessel.Parts.Count != 0)
                    avgHitPoint /= this.vessel.Parts.Count;
                else
                    avgHitPoint = 0;
                this._shieldMaxValue = this._shieldBaseValue + (int)avgHitPoint;
                this._shieldCurrentValue = this._shieldMaxValue * 0.01f * this._ShieldCompletenessPercent;
                this.part.Modules.GetModule<HitpointTracker>().Armor = 500f;

                //材质
                var color = materialDefault.GetColor("_TintColor");
                float time = (float)Planetarium.GetUniversalTime();
                if (materialDefault.GetFloat("_Brightness") > 1f)
                {
                    //0.5s秒衰减
                    materialDefault.SetFloat("_Brightness", materialDefault.GetFloat("_Brightness") - Time.fixedDeltaTime * (float)Planetarium.TimeScale / 0.5f);
                    this.isHurting = true;
                }
                else
                {
                    materialDefault.SetFloat("_Brightness", 1f);
                    this.isHurting = false;
                }
                materialDefault.SetFloat("_Times", time);
                materialDefault.SetFloat("_Pan_Speed", 4 + (float)Planetarium.TimeScale);
                materialDefault.SetColor("_TintColor", new Color(color.r, color.g, color.b, this.ShieldTransparency));
                float temp = 1f - this._ShieldCompletenessPercent * 0.01f;
                if (temp > 0.9f) temp = 0.9f;
                materialDefault.SetFloat("_DissolveSpeed", temp);
                mr.material = materialDefault;
                if (this._shieldDisplayIntensity <= 0.0001f)
                    mr.enabled = false;
            }
        }

        public double GetAvailableResource(string name)
        {
            double availableShieldField = 0;
            foreach (var _part in this.vessel.Parts)
            {
                var resource = _part.Resources.Get(name);
                if (resource != null &&
                resource.amount > 0 &&
                  resource.isTweakable)
                {
                    availableShieldField += resource.amount;
                }
            }
            return availableShieldField;
        }

        public void AddDamage(float damage, bool isCollider = false)
        {
            Material mat = this.GetMaterial();
            if (mat != null)
            {
                mat.SetFloat("_Brightness", 3f);
            }
            if (audioSource)
            {
                if (!isCollider)
                {
                    audioSource.clip = attackShield;
                    audioSource.PlayOneShot(attackShield);
                }
            }
            float tempHitPoint = this._shieldCurrentValue - damage;
            if (tempHitPoint > 0)
            {
                this._ShieldCompletenessPercent = tempHitPoint / this._shieldMaxValue * 100f;
            }
            else
            {
                this._ShieldCompletenessPercent = 0f;
                this.Events["Deactivate"].Invoke();
            }
        }

        public Material GetMaterial()
        {
            if (this._switchShield)
            {
                var shield = this.part.FindModelTransform(shieldName);
                if (shield == null)
                {
                    Debug.LogError("[ModuleShieldGenerator] Wrong shield name!");
                    return null;
                }
                var mr = shield.GetComponent<MeshRenderer>();
                return mr.material;
            }
            else
                return null;
        }

        public void EditorUpdate()
        {
            if (HighLogic.LoadedScene == GameScenes.EDITOR && _isSingle == true)
            {
                var shield = this.part.FindModelTransform(shieldName);
                if (shield == null)
                {
                    Debug.LogError("[ModuleShieldGenerator] Wrong shield name!");
                    return;
                }
                var shieldCollider = shield.GetComponent<ShieldCollider>();
                if (shieldCollider != null)
                {
                    DestroyImmediate(shieldCollider);
                }
                var _audioSource = shield.GetComponent<AudioSource>();
                if (_audioSource != null)
                {
                    DestroyImmediate(_audioSource);
                }
                shield.GetComponent<Collider>().enabled = true;
                int count = 0;
                double editorAvgHitPoint = 0;
                float editorTotalMass = 0;

                List<GameObject> parts = new List<GameObject>();
                foreach (var item in Part.allParts)
                {
                    if (item.isActiveAndEnabled && !item.isLaunchClamp())
                    {
                        count++;
                        var moduleHit = item.Modules.GetModule<HitpointTracker>();
                        if (moduleHit)
                        {
                            editorAvgHitPoint += moduleHit.Hitpoints;
                        }
                        editorTotalMass += item.mass;
                        parts.Add(item.gameObject);
                    }
                }

                editorAvgHitPoint /= count;
                this._shieldBaseValue = (100000 + 10000 * (int)(editorTotalMass / 2000f));
                this._shieldMaxValue = this._shieldBaseValue + (int)(editorAvgHitPoint);
                this._ShieldCompletenessPercent = 100f;
                this.part.Modules.GetModule<HitpointTracker>().Armor = 500f;
                this.effect = new EffectBase(this.part.vessel);
                var tempPos = shield.localPosition;
                shield.position = this.effect.EffectWorldPosition;
                this._localCenter = shield.localPosition;
                shield.localPosition = tempPos;
                var tempScale = new Vector3(0.001f, 0.001f, 0.001f);
                GameObject tempObj = new GameObject();
                tempObj.transform.position = Vector3.zero;
                tempObj.transform.eulerAngles = Vector3.zero;
                tempObj.transform.localScale = new Vector3(1, 1, 1);
                var parent = shield.parent;
                shield.parent = tempObj.transform;
                if (EditorDriver.editorFacility == EditorFacility.SPH)
                    shield.localEulerAngles = new Vector3(90, 0, 0);
                if (EditorDriver.editorFacility == EditorFacility.VAB)
                    shield.localEulerAngles = new Vector3(0, 0, 0);
                shield.localScale = this.effect.EffectWorldScale * (1f + 0.01f * clearanceRatio);
                shield.parent = parent;
                DestroyImmediate(tempObj);
                this._localSize = shield.localScale;
                this._localEuler = shield.localEulerAngles;
                shield.localScale = tempScale;
            }
        }

        public void OnDestroy()
        {
            if (ModuleShieldGeneratorController.shield != null)
                ModuleShieldGeneratorController.shield = null;
        }
    }
}
