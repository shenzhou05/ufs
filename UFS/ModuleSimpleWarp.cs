﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UFS
{
    public class ModuleSimpleWarp : PartModule
    {
        public bool inWarp = false;

        //载具初始速度方向
        private Vector3d speedVector;
        private Vector3d warpVector;

          private static GameObject WarpPrefab=null;
        //  private GameObject InstantiateWarpPrefab;
        public override void OnStart(StartState state)
        {
            base.OnStart(state);
            inWarp = false;
            if (WarpPrefab == null)
            {
                WarpPrefab = ResourcesController.test.LoadAsset("WarpPrefab") as GameObject;
            }
        }

        public override void OnFixedUpdate()
        {
            base.OnFixedUpdate();
            if (HighLogic.LoadedSceneIsEditor)
                return;
            if (Input.GetKeyDown(KeyCode.L))
            {
                ActivateWarpDrive();
            }
            UpdateWarpSpeed();
        }

        public void ActivateWarpDrive()
        {
            if (this.vessel.altitude <= getMaxAtmosphericAltitude(this.vessel.mainBody) &&
                this.vessel.mainBody.flightGlobalsIndex != 0)
            {
                ScreenMessages.PostScreenMessage("Cannot activate warp drive within the atmosphere!", 7.0f);
                return;
            }

            /* if (drivesEfficiencyRatio < 1)
             {
                 ScreenMessages.PostScreenMessage("Not enough drives power to initiate warp!", 7.0f);
                 return;
             }*/
            InitiateWarp();
        }

        public static double getMaxAtmosphericAltitude(CelestialBody body)
        {
            if (!body.atmosphere) return 0;
            return body.atmosphereDepth;
        }

        private void InitiateWarp()
        {
            inWarp = true;
            var cele = (CelestialBody)vessel.targetObject;
            if (cele == null) return;
            this.preMainBody = this.vessel.mainBody;
            vessel.transform.LookAt(cele.transform);
            vessel.transform.Rotate(new Vector3(90, 0, 0));
            warpVector = new Vector3d(part.transform.up.x, part.transform.up.z, part.transform.up.y) * 299792458;

            speedVector = vessel.orbit.vel;
            //旋转载具朝向
            vessel.GoOnRails();
            vessel.orbit.UpdateFromStateVectors(vessel.orbit.pos, warpVector, vessel.orbit.referenceBody, Planetarium.GetUniversalTime());
            vessel.GoOffRails();

            //--------------------------特效----------------------------
            var warpEffect = part.FindModelTransform("warpEffect");
            var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.parent = warpEffect;
            sphere.transform.localPosition = Vector3.zero;
            sphere.transform.localScale = Vector3.one * 10f; 

            Debug.Log("warpEffect world postion: " + sphere.transform.position);
            int count = 0;
            List<GameObject> parts = new List<GameObject>();
            foreach (var item in Part.allParts)
            {
                if (item.isActiveAndEnabled && !item.isLaunchClamp())
                {
                    count++;
                    parts.Add(item.gameObject);
                }
            }

            List<float> collidersXMax = new List<float>();
            List<float> collidersXMin = new List<float>();
            List<float> collidersYMax = new List<float>();
            List<float> collidersYMin = new List<float>();
            List<float> collidersZMax = new List<float>();
            List<float> collidersZMin = new List<float>();
            List<Collider> colliderArray = new List<Collider>();

            foreach (var item in parts)
            {
                colliderArray.AddRange(item.GetComponentsInChildren<Collider>());
            }

            foreach (var item in colliderArray)
            {
                if (item.gameObject.name == "obj_girder_mesh" && item.gameObject.transform.parent.parent.parent.name == "launchclamp")
                    continue;
                if (item.gameObject.name == "obj_ground_mesh" && item.gameObject.transform.parent.parent.name == "launchclamp")
                    continue;
                if (item.gameObject.name == "base" && item.gameObject.transform.parent.name == "launchclamp")
                    continue;
                collidersXMax.Add(item.bounds.center.x + item.bounds.extents.x);
                collidersXMin.Add(item.bounds.center.x - item.bounds.extents.x);
                collidersYMax.Add(item.bounds.center.y + item.bounds.extents.y);
                collidersYMin.Add(item.bounds.center.y - item.bounds.extents.y);
                collidersZMax.Add(item.bounds.center.z + item.bounds.extents.z);
                collidersZMin.Add(item.bounds.center.z - item.bounds.extents.z);
            }

            collidersXMax.Sort();
            collidersXMin.Sort();
            collidersYMax.Sort();
            collidersYMin.Sort();
            collidersZMax.Sort();
            collidersZMin.Sort();

            if (collidersXMax.Count < 1) return;
            Vector3 center = new Vector3(
              (collidersXMax[collidersXMax.Count - 1] + collidersXMin[0]) / 2f,
              (collidersYMax[collidersYMax.Count - 1] + collidersYMin[0]) / 2f,
              (collidersZMax[collidersZMax.Count - 1] + collidersZMin[0]) / 2f);
            Vector3 extents = new Vector3(collidersXMax[collidersXMax.Count - 1] - center.x, collidersYMax[collidersYMax.Count - 1] - center.y, collidersZMax[collidersZMax.Count - 1] - center.z);
            var size = 2f * extents.magnitude;
            float clearanceRatio = 1f;
            size *= (1f + 0.01f * clearanceRatio);

            //             var tempPos = shield.localPosition;
            //             shield.position = center;
            //             this._localCenter = shield.localPosition;
            //             shield.localPosition = tempPos;
            //             var tempScale = new Vector3(0.001f, 0.001f, 0.001f);
            //             GameObject tempObj = new GameObject();
            //             tempObj.transform.position = Vector3.zero;
            //             tempObj.transform.eulerAngles = Vector3.zero;
            //             tempObj.transform.localScale = new Vector3(1, 1, 1);
            //             var parent = shield.parent;
            //             shield.parent = tempObj.transform;
            //             if (EditorDriver.editorFacility == EditorFacility.SPH)
            //                 shield.localEulerAngles = new Vector3(90, 0, 0);
            //             if (EditorDriver.editorFacility == EditorFacility.VAB)
            //                 shield.localEulerAngles = new Vector3(0, 0, 0);
            //             shield.localScale = new Vector3(size, size, size);
            //             shield.parent = parent;
            //             DestroyImmediate(tempObj);
            //             this._localSize = shield.localScale;
            //             this._localEuler = shield.localEulerAngles;
            //             shield.localScale = tempScale;
            // if (!WarpPrefab) { Debug.Log("fuck!"); return; 
            /*  this.InstantiateWarpPrefab = Instantiate(WarpPrefab, Vector3.zero, Quaternion.identity,this.transform);
              this.InstantiateWarpPrefab.transform.localScale = 0.01f * Vector3.one;
              this.InstantiateWarpPrefab.SetActive(true);*/
        }

        public void DeactivateWarpDrive()
        {
            inWarp = false;
            vessel.GoOnRails();
            vessel.orbit.UpdateFromStateVectors(vessel.orbit.pos, speedVector, vessel.orbit.referenceBody, Planetarium.GetUniversalTime());
            vessel.GoOffRails();
            vessel.angularVelocity = Vector3d.zero;
            //  this.InstantiateWarpPrefab.SetActive(false);
        }

        private CelestialBody preMainBody;

        private bool CheckMainBody()
        {
            return (this.vessel.mainBody != this.preMainBody) || this.vessel.targetObject == null;
        }

        public void UpdateWarpSpeed()
        {
            if (!inWarp) return;
            var gravityPull = FlightGlobals.getGeeForceAtPosition(vessel.GetWorldPos3D()).magnitude;
            if (CheckMainBody() && gravityPull > 0f)
            {
                DeactivateWarpDrive();
                return;
            }

            //实时旋转载具对准目标星球
            var cele = (CelestialBody)vessel.targetObject;
            vessel.transform.LookAt(cele.transform);
            vessel.transform.Rotate(new Vector3(90, 0, 0));
            warpVector = new Vector3d(part.transform.up.x, part.transform.up.z, part.transform.up.y) * 299792458;
            vessel.GoOnRails();
            vessel.orbit.UpdateFromStateVectors(vessel.orbit.pos, warpVector, vessel.orbit.referenceBody, Planetarium.GetUniversalTime());
            vessel.GoOffRails();
        }
    }
}
