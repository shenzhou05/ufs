﻿using UnityEngine;
using System;
using System.Collections;

public class ScriptLaunch
{
    [Serializable]
    public class Value
    {
        public string name;
        public GameObject obj;
    }
    public Value[] values;
}
