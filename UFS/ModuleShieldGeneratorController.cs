﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using KSP;

namespace UFS
{
    [KSPAddon(KSPAddon.Startup.EditorAny, false)]
    public class ModuleShieldGeneratorController : MonoBehaviour
    {
        public static ModuleShieldGenerator shield;

        public void Update()
        {
            if (shield)
                shield.EditorUpdate();
        }
    }
}
